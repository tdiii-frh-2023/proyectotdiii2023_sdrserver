#include <stdio.h>
#include <pthread.h>

#define FILENAME "data.txt"
#define FILE_SIZE 100

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t write_ready = PTHREAD_COND_INITIALIZER;
pthread_cond_t read_ready = PTHREAD_COND_INITIALIZER;

FILE* file;
int write_position = 0;
int read_position = 0;

void* write_thread(void* arg) {
    for (int i = 0; i < FILE_SIZE; i++) {
        pthread_mutex_lock(&mutex);
        while (write_position >= 50) {
            pthread_cond_wait(&write_ready, &mutex);
        }

        fputc('A', file);
        fflush(file); // Flush the data to the file
        write_position++;

        if (write_position == 50) {
            pthread_cond_signal(&read_ready);
        }

        pthread_mutex_unlock(&mutex);
    }

    fclose(file);
    pthread_cond_signal(&read_ready);
    return NULL;
}

void* read_thread(void* arg) {
    for (int i = 0; i < FILE_SIZE; i++) {
        pthread_mutex_lock(&mutex);
        while (read_position < 50) {
            pthread_cond_wait(&read_ready, &mutex);
        }

        char c = fgetc(file);
        if (c != EOF) {
            printf("Read: %c\n", c);
        }

        read_position++;

        if (read_position == 100) {
            pthread_cond_signal(&write_ready);
        }

        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}

int main() {
    file = fopen(FILENAME, "w+");
    if (file == NULL) {
        perror("Unable to open file");
        return 1;
    }

    pthread_t writer, reader;

    pthread_create(&writer, NULL, write_thread, NULL);
    pthread_create(&reader, NULL, read_thread, NULL);

    pthread_join(writer, NULL);
    pthread_join(reader, NULL);

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&write_ready);
    pthread_cond_destroy(&read_ready);

    fclose(file); // Close the file

    return 0;
}
