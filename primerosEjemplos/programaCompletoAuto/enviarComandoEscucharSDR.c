#include <stdio.h>
#include <stdlib.h>

int main() {
    const char * escucharGuardar = "rtl_fm -M wbfm -f 93.3M | tee output.bin > /dev/null";

    int return_code = system(escucharGuardar);

    if (return_code) {
        printf("Error\n");
    }

    return 0;
}
