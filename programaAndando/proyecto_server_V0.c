/*
Uso: llamar a server y mandar como argumento la IP de broadcast
Proceso LECTURA: Lee la SDR escribe datos en archivos .wav. Alterna entre 2 archivos.
Proceso Accept: Se queda esperando conexiones nuevas por TCP
Proceso UDP: Envía la información de los archivos por UDP. 
Proceso TCP: Se genera conexión TCP. Acepta cliente y le recibe el puerto UDP. Espera comandos.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

//Buf size: LENGTH*RATE*SIZE*CHANNELS/8
#define DATA_SIZE 64000
#define TCP_PORT 2225
#define BACKLOG 10
#define WAV_1 "output_1.wav" 
#define WAV_2 "output_2.wav" 

int set_tcp (void);                 //Creates TCP Socket
void tcp_process (int);             //Accepts TCP
int set_udp (void);                 //Creates TCP Socket
void udp_process (int, struct sockaddr_in); //Accepts TCP
void handler_usr1 (int);
void handler_usr2 (int);
void handler_child (int);
void handler_int (int val);

char buf[64000];  //This buffer holds the digitized audio
int childs_done = 0;   //Number of Childs that finished reading
int new_data_1 = 0;   //1 if children have to read new data in output_1.wav
int new_data_2 = 0;   //1 if children have to read new data in output2.wav
int child_id = 0;
char * data;

int main()
{
    int socket_tcp, socketfd_udp, newfd;
    struct sockaddr_in their_addr_udp, their_addr_tcp;
    int udp_port, command, numbytes = 1;
    int sin_size = sizeof(struct sockaddr);
    const char * record_iq = "rtl_fm -M iq -f 89.1M -s 170K -g 20 -t 100 -A fast -r 32k - | dd bs=8k count=30 > output.iq";
    const char * create_wav_1 = "sox -t raw -r 16k -e signed-integer -b 16 -c 2 -V1 output.iq output_1.wav";
    const char * create_wav_2 = "sox -t raw -r 16k -e signed-integer -b 16 -c 2 -V1 output.iq output_2.wav";

    signal (SIGINT, handler_int);

    if (!(child_id = fork()))
    {
        //Capturo SIGUSR2
        signal (SIGUSR2, handler_usr2);
        signal (SIGUSR2, handler_usr1);

        //Set TCP
        socket_tcp = set_tcp();
        
        //Set UDP
        socketfd_udp = set_udp();

        //Accept
        if ((newfd = accept(socket_tcp, (struct sockaddr *) &their_addr_tcp, &sin_size)) == -1)
        {
            perror("accept");
            exit(1);
        }

        if(!(child_id = fork()))
        {
            printf("\x1b[32mConexion Establecida\x1b[0m\n");

            if ((recv(newfd, &udp_port, sizeof(int), 0)) == -1)
            {
                perror("Receive Port UDP");
                exit(1);
            }
            printf("\x1b[32mRecibido Puerto UDP: %d\x1b[0m\n",udp_port);

            if(!(child_id = fork()))
            {
                //Receive Commands TCP
                tcp_process(newfd);

                printf("\x1b[31mConexion perdida desde %s\x1b[0m\n", inet_ntoa(their_addr_tcp.sin_addr));
                kill(SIGKILL, getppid());
                close (newfd);
                exit(0);
            }

            //Receive Port
            their_addr_udp = their_addr_tcp;
            their_addr_udp.sin_port = htons(udp_port);

            //Send UDP
            while(1)
            {
                udp_process(socketfd_udp, their_addr_udp);
            }
        }
    }

    
    while(1)
    {
        //Get Audio
        system(record_iq);
        system(create_wav_1);

        //New data available
        kill (child_id, SIGUSR1);

        //Get Audio
        system(record_iq);
        system(create_wav_2);

        //New data available
        kill (child_id, SIGUSR2);
    }

    return 0;
}


int set_tcp (void)
{
    int socketfd_tcp;
    struct sockaddr_in my_addr_tcp;
    
    //Create Socket
    printf("\x1b[32mTCP Creandose...\x1b[0m\n");
    if ((socketfd_tcp = socket(AF_INET, SOCK_STREAM ,0)) == -1) 
    {
        perror("socket TCP");
        exit(1);
    }
    printf("\x1b[32mTCP Creado...\x1b[0m\n");
    
    //set my__addr_tcp
    my_addr_tcp.sin_family = AF_INET;
    my_addr_tcp.sin_port = htons(TCP_PORT);
    my_addr_tcp.sin_addr.s_addr = INADDR_ANY;
    bzero(& (my_addr_tcp.sin_zero), 8);
    
    //Bind Port
    if (bind(socketfd_tcp, (struct sockaddr *) & my_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("bind TCP");
        exit(1);
    }
    
    //Indicate Queue size
    if (listen(socketfd_tcp, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }
    
    return socketfd_tcp;
}

void tcp_process (int newfd)
{
    int command, numbytes = 1;
        
    //Wait for commands
    while(numbytes != 0)
    {
        if (numbytes = (recv(newfd, &command, sizeof(int), 0)) == -1)
        {
            perror("recv TCP");
            exit(1);
        }

        //ACA SE AGREGA CODIGO PARA PROCESAR COMANDOS
    }

    return;
}  

int set_udp ()
{
    int socketfd_udp;

    //Create Socket
    if ((socketfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket UDP");
        exit(1);
    }

    return socketfd_udp;
}

void udp_process (int socket_udp, struct sockaddr_in their_addr_udp)
{   
    int i, bytes_leidos;
    char * input_file;
    FILE * fd;

    if (new_data_1 == 1 || new_data_2 == 1)
    {
        //Select file
        if (new_data_1 == 1)
        {
            strcpy(input_file, WAV_1);
            new_data_1 = 0;
        }
        else
        {
            strcpy(input_file, WAV_2);
            new_data_2 = 0;
        }

        //Open file
        if ((fd = fopen(input_file, "rb")) == NULL)
        {
            perror("Open File");
            exit(1);
        }

        //Send audio via UDP
        while ((bytes_leidos = fread(buf, 1, DATA_SIZE, fd)) > 0)
        {
            if ((sendto(socket_udp, buf, bytes_leidos, 0, (struct sockaddr *)&their_addr_udp, sizeof(struct sockaddr))) == -1)
            {
                perror("sendto");
                return;
            }

            printf("\033[31mEnviando %d bytes\033[0m\n", bytes_leidos);
        }

        //Close file
        fclose(fd);
    }
    
    return;
}


void handler_usr1 (int val)
{
    new_data_1 = 1;
    return;
}

void handler_usr2 (int val)
{
    new_data_2 = 1;
    return;
}

void handler_child (int val)
{
    wait(0);
    return;
}

void handler_int (int val)
{
    if (child_id)
        kill(child_id, SIGINT);
    printf("Cerrando Proceso %d...\n", getpid());
    exit(0);
}