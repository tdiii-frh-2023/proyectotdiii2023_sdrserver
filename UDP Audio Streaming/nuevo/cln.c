#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <termios.h>
#include <string.h>
#include <vlc/vlc.h>

#define PUERTO_TCP (int)10103
#define BUFFER_TAM (int)59000

char buffer[BUFFER_TAM];

FILE * archivoWAV;

int main(){
    
    int socket_TCP;
    
    int sin_tam = sizeof(struct sockaddr);
    
    struct sockaddr_in direccion_servidor;
    struct hostent * entidad_anfitrion;
    
    entidad_anfitrion = gethostbyname("localhost");
    
    if (entidad_anfitrion == NULL) {
        fprintf(stderr, "Error al obtener la información del host\n");
        exit(1);
    }
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    
    if ( socket_TCP == -1 ){
        printf("socket\n\n");
        exit(1);
    }
    
    direccion_servidor.sin_family = AF_INET;
    direccion_servidor.sin_port = htons(PUERTO_TCP);
    direccion_servidor.sin_addr = *(struct in_addr *)entidad_anfitrion->h_addr;
    bzero(&(direccion_servidor.sin_zero),8);
    
    int error;
    
    printf("Preparando connect\n");
    
    error = connect(
        socket_TCP,
        (struct sockaddr *) &direccion_servidor,
        sin_tam
                );
    
    if ( error == -1 ){
        printf("connect\n\n");
        exit(1);
    }
    
    int bytes_recibidos;
    
    //const char * escucharRecibir = "netcat localhost 12121 | ffplay -";
    
    archivoWAV = fopen("audio.wav","w+b");
    
    //int return_code = system(escucharRecibir);
    
    //while(1);
    
    int paquetes = 1;
    
    while( paquetes  ){
        bytes_recibidos = recv(
            socket_TCP,
            buffer,
            BUFFER_TAM,
            0
                            );
        
        if(bytes_recibidos){
            printf("Recibidos %d bytes\n",
                   bytes_recibidos
            );
        }
        
        
        fwrite(
            buffer,
            bytes_recibidos,
            1,
            archivoWAV            
        );
        
        printf("Archivo recibido");
        
            libvlc_instance_t *vlc;
    libvlc_media_player_t *media_player;
    libvlc_media_t *media;

    // Inicializar la instancia de libvlc
    vlc = libvlc_new(0, NULL);

    // Crear un objeto de media a partir del archivo MP3
    media = libvlc_media_new_path(vlc, "recibir.mp3");

    // Crear un reproductor de medios
    media_player = libvlc_media_player_new_from_media(media);

    // Liberar el objeto de medios, ya que ya no lo necesitamos
    libvlc_media_release(media);

    // Establecer el reproductor de medios al estado de reproducción
    libvlc_media_player_play(media_player);

    // Esperar un tiempo suficiente para que VLC inicie la reproducción (puedes ajustar esto según tus necesidades)
    sleep(5);

    // Detener la reproducción y liberar recursos
    libvlc_media_player_stop(media_player);
    libvlc_media_player_release(media_player);
    libvlc_release(vlc);
        
        /*
        printf("Enviado %d (%d bytes)\n",
               mensaje,
               sizeof(mensaje)
        );
        */
        
        paquetes ++;
    }
    
    fclose(archivoWAV);
    close(PUERTO_TCP);
    
    



    return 0;
}
