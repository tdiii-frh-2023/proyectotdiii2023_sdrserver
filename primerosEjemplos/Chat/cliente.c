#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <termios.h>
#include <string.h>

#define PUERTO_TCP (int)10103
#define BUFFER_TAM (int)1024
#define CLIENTES_MAX (int)5

char buffer[BUFFER_TAM];

int main(){
    
    int socket_TCP;
    
    int sin_tam = sizeof(struct sockaddr);
    
    struct sockaddr_in direccion_servidor;
    struct hostent * entidad_anfitrion;
    
    entidad_anfitrion = gethostbyname("localhost");
    
    if (entidad_anfitrion == NULL) {
        fprintf(stderr, "Error al obtener la información del host\n");
        exit(1);
    }
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    
    if ( socket_TCP == -1 ){
        printf("socket\n\n");
        exit(1);
    }
    
    direccion_servidor.sin_family = AF_INET;
    direccion_servidor.sin_port = htons(PUERTO_TCP);
    direccion_servidor.sin_addr = *(struct in_addr *)entidad_anfitrion->h_addr;
    bzero(&(direccion_servidor.sin_zero),8);
    
    int error;
    
    printf("Preparando connect\n");
    
    error = connect(
        socket_TCP,
        (struct sockaddr *) &direccion_servidor,
        sin_tam
                );
    
    if ( error == -1 ){
        printf("connect\n\n");
        exit(1);
    }
    
    int bytes_recibidos = recv(
        socket_TCP,
        buffer,
        BUFFER_TAM,
        0
                        );
    
    if(bytes_recibidos){
        buffer[bytes_recibidos] = '\0';
        
        printf("Recibidos %d bytes \tContenido : %s\n",
                bytes_recibidos,
                buffer
        );
    }
    
    fgets(buffer,BUFFER_TAM,stdin);
    send(
        socket_TCP,
        buffer,
        strlen(buffer),
        0
    );
    
    if(!fork()){
        //hijo
        //envio mensajes por TCP
        while(1){
            bzero(buffer,BUFFER_TAM);
            fgets(buffer,BUFFER_TAM,stdin);
            send(
                socket_TCP,
                buffer,
                strlen(buffer),
                0
            );
        }
    }
    if(!fork()){
        //hijo
        //recibo mensajes por TCP
        while(1){
            bytes_recibidos = recv(
                socket_TCP,
                buffer,
                BUFFER_TAM,
                0
                                );
            
            if(bytes_recibidos){
                buffer[bytes_recibidos] = '\0';
                
                /*printf("Recibidos %d bytes \tContenido : %s\n",
                        bytes_recibidos,
                        buffer
                );
                */
                printf("%s",buffer);
            }
        }
    }
    if(!fork()){
        //hijo
        //recibo mensajes por UDP
        while(1);
    }
    //padre, proceso libre
    while(1);

    return 0;
}
