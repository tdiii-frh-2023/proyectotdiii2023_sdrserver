#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rtl-sdr.h>

#define BUFFER_SIZE (16 * 1024)
#define SAMPLE_RATE 2.048e6
#define CENTER_FREQUENCY 433e6
#define DURATION_SECONDS 5
#define OUTPUT_FILE "output.bin"

int main(int argc, char **argv) {
    int result;
    int device_index = 0;
    rtlsdr_dev_t *dev = NULL;
    unsigned char *buffer = malloc(BUFFER_SIZE);

    // Initialize the RTL-SDR device
    result = rtlsdr_open(&dev, device_index);
    if (result < 0) {
        fprintf(stderr, "Error opening the RTL-SDR device: %d\n", rtlsdr_get_usb_strings(result));
        return 1;
    }

    // Configure the RTL-SDR device
    result = rtlsdr_set_sample_rate(dev, SAMPLE_RATE);
    result |= rtlsdr_set_center_freq(dev, CENTER_FREQUENCY);
    if (result < 0) {
        fprintf(stderr, "Error configuring the RTL-SDR device: %d\n", rtlsdr_get_usb_strings(result));
        rtlsdr_close(dev);
        return 1;
    }

    // Open the output binary file for writing
    FILE *output = fopen(OUTPUT_FILE, "wb");
    if (output == NULL) {
        fprintf(stderr, "Error opening output file for writing\n");
        rtlsdr_close(dev);
        return 1;
    }

    // Read samples and save them to the output file for the specified duration
    time_t start_time = time(NULL);
    while (1) {
        if (time(NULL) - start_time >= DURATION_SECONDS) {
            break;
        }

        int num_samples;
        result = rtlsdr_read_sync(dev, buffer, BUFFER_SIZE, &num_samples);
        if (result < 0) {
            fprintf(stderr, "Error reading samples: %d\n", rtlsdr_get_usb_strings(result));
            break;
        }

        fwrite(buffer, 1, num_samples, output);
    }

    // Close the output file and cleanup
    fclose(output);
    rtlsdr_close(dev);
    free(buffer);

    return 0;
}
