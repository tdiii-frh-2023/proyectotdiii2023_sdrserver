#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PUERTO 10101
#define BUFMAX 1024

void recibir_archivo(const char *nombre_archivo, int descriptor_socket) {
    FILE *archivo = fopen(nombre_archivo, "wb");

    if (archivo == NULL) {
        perror("Error al crear el archivo");
        exit(EXIT_FAILURE);
    }

    char buffer[BUFMAX];
    size_t bytes_recibidos;

    // Bucle para recibir datos del cliente
    while ((bytes_recibidos = recvfrom(descriptor_socket, buffer, sizeof(buffer), 0, NULL, NULL)) > 0) {
        // Verificar si es el marcador de fin de archivo
        if (bytes_recibidos == 1 && buffer[0] == '\0') {
            break;  // Fin del archivo
        }

        // Escribir datos en el archivo
        fwrite(buffer, 1, bytes_recibidos, archivo);
    }

    fclose(archivo);
}

int main() {
    // Configurar el servidor UDP
    int descriptor_socket = socket(AF_INET, SOCK_DGRAM, 0);

    if (descriptor_socket == -1) {
        perror("Error al crear el socket");
        exit(EXIT_FAILURE);
    }

    // Configurar la dirección del servidor
    struct sockaddr_in direccion_servidor;
    memset(&direccion_servidor, 0, sizeof(direccion_servidor));
    direccion_servidor.sin_family = AF_INET;
    direccion_servidor.sin_addr.s_addr = htonl(INADDR_ANY);
    direccion_servidor.sin_port = htons(PUERTO);

    // Vincular el socket a la dirección y puerto
    if (bind(descriptor_socket, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor)) == -1) {
        perror("Error al vincular el socket");
        close(descriptor_socket);
        exit(EXIT_FAILURE);
    }

    printf("Escuchando en el puerto %d...\n", PUERTO);

    // Recibir el archivo del cliente
    recibir_archivo("archivo_recibido.mp3", descriptor_socket);

    printf("Archivo recibido correctamente.\n");

    // Cerrar el socket
    close(descriptor_socket);

    return 0;
}
