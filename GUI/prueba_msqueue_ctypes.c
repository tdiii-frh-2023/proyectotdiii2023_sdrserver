#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <sys/msg.h>

#define BUF_SIZE 20

//gcc -fPIC -shared -o clibrary.so prueba_ctypes.c para generar el dll

struct msgbuf{
    long mtype;
    char mdata[BUF_SIZE];
}typedef msgbuf_t;

void receive_data(int msgqid)
{
    msgbuf_t buf;

    if(!fork()){
        msgrcv(msgqid, &buf, sizeof(msgbuf_t), 2, 0);
        printf("c process: I received %s \n", buf.mdata);
        msgrcv(msgqid, &buf, sizeof(msgbuf_t), 2, 0);
        printf("c process: I received %s \n", buf.mdata);
        msgrcv(msgqid, &buf, sizeof(msgbuf_t), 3, 0);
        printf("c process: I received %s \n", buf.mdata);
    }
}