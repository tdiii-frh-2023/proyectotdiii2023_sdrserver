/*
Uso: llamar a cliente y mandar como argumento la IP (127.0.0.0)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/shm.h>
#include <signal.h>

#define LENGTH 1    //How many seconds of speech to store
#define RATE 20000  //Sampling Rate
#define SIZE 8      //Sample Size
#define CHANNELS 2  //Channels
#define TCP_PORT 2225
#define AUDIO_SIZE 245804
#define DATA_SIZE 64000


int set_tcp (struct sockaddr_in);
int set_udp_port (int);
void receive_udp (int);
void shared_memory (void);
void handler_int (int val);
void play_audio(void);
void handler_usr1 (int);



char * data;
int child_id;
int new_data = 0;

int main (int argc, char * argv[])
{
    int socket_tcp, socket_udp;
    struct sockaddr_in their_addr_tcp; //Informacion del server
    struct hostent * he; //Direccion a donde se conecta
    int udp_port = 0;

    signal (SIGINT, handler_int);
    signal (SIGUSR2, handler_usr1);

    //Reviso cantidad argumentos
    if (argc != 2)
    {
        perror("argumentos");
        exit(1);
    } 
    
    //Convierto nombre Host a IP
    if ((he = gethostbyname(argv[1]))== NULL)
    {
        perror("gethostbyname");
        exit(1);
    }
    
    //Server Information
    their_addr_tcp.sin_family = AF_INET;
    their_addr_tcp.sin_port = htons(TCP_PORT);
    their_addr_tcp.sin_addr = *(struct in_addr *)he->h_addr;
    bzero(&(their_addr_tcp.sin_zero), 8);

    //Shared Memory
    shared_memory();
    
    //Set TCP
    socket_tcp = set_tcp(their_addr_tcp);
    
    //Set UDP port
    socket_udp = set_udp_port(socket_tcp);
    
    if(!(child_id = fork()))
    {
        play_audio();
        exit(0);
    }

    receive_udp(socket_udp);
      
    close(socket_tcp);
    close(socket_udp);

    return 0;
}

void shared_memory (void)
{
    key_t key_sh;
    int shmid;

    //Create Shared Memory
    if ((key_sh = ftok("proyecto_server.c", 'R')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((shmid = shmget (key_sh, 2*AUDIO_SIZE, 0644 | IPC_CREAT)) == -1)
    {
        perror("shmget");
        exit(1);
    }

    if ((data = shmat(shmid, 0, 0)) == (char *) (-1))
    {
        perror("shmat");
        exit(1);
    }

    return;
}

int set_tcp (struct sockaddr_in their_addr_tcp)
{
    int socket_tcp;
    
    //Create Socket
    if ((socket_tcp = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket TCP");
        exit(1);
    }
    
    //Connect
    if (connect(socket_tcp, (struct sockaddr *) & their_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("connect TCP");
        exit(1);
    }

    printf ("\e[32mPedido de Conexion hecho\e[0m\n");
    
    return socket_tcp;
} 

int set_udp_port(int socket_tcp)
{
    int udp_port = 10000; //Initial UDP Port
    struct sockaddr_in my_addr_udp;
    int socket_udp;
    
    //Create Socket
    if ((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    
    //Set my_addr_udp
    my_addr_udp.sin_family = AF_INET;
    my_addr_udp.sin_port = htons(udp_port);
    my_addr_udp.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr_udp.sin_zero), 8);
    
    //Bind
    while (bind(socket_udp, (struct sockaddr *)&my_addr_udp, sizeof(struct sockaddr))  == -1)
    {
        my_addr_udp.sin_port = htons(udp_port++);
    }
    
    //Inform Server
    if (send(socket_tcp, &udp_port, sizeof(int), 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    printf ("\e[34mPuerto UDP: %d\e[0m\n", udp_port);

    return socket_udp;
}

void receive_udp(int socket_udp)
{
    char buf[AUDIO_SIZE];
    int i, bytes_acum, bytes_recibidos;
    int sinsize = sizeof(struct sockaddr);
    struct sockaddr_in their_addr;

    while(1)
    {
        //Reset Variables
        bzero(buf, AUDIO_SIZE);
        i = 0;
        bytes_acum = 0;

        //Receive Audio via UDP
        do
        {
            bytes_recibidos = recvfrom(socket_udp, buf+DATA_SIZE*i, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
            printf("Recibidos %d bytes \n", bytes_recibidos);
            i++;
            bytes_acum += bytes_recibidos;
        }while(bytes_recibidos >= DATA_SIZE);
        
        strcpy(buf, data);
        kill (child_id, SIGUSR1);
    }
    
    return;
}

void play_audio(void)
{
    FILE * aplay;
    const char * listen = "aplay -f S16_LE -r 24000 -c 1 -"; //Comando Reproduccion
    char buf[AUDIO_SIZE];
    int i = 0;

    while(1)
    {
        if(new_data)
        {
            new_data = 0;
            strcpy(buf, data);

            //Open aplay       
            if ((aplay = popen(listen, "w")) == NULL) 
            {
                printf("Error al abrir aplay.\n");
                return;
            }

            //Play Audio
            if(i=0)
            {
                fwrite(buf, sizeof(char), AUDIO_SIZE, aplay);
                i++;
            }
            else
            {
                fwrite(buf+AUDIO_SIZE, sizeof(char), AUDIO_SIZE, aplay);
                i = 0;
            }

            //Close
            pclose(aplay);
        }

    }
}

void handler_int (int val)
{
    if (child_id)
        kill(child_id, SIGINT);
    printf("Cerrando Proceso %d...\n", getpid());
    exit(0);
}

void handler_usr1 (int val)
{
    new_data = 1;
    return;
}