/*********************************************
PARA COMPILAR: gcc enviar.c -o nombre

EJECUTAR CON ./nombre localhost
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
extern int h_errno;

#define DATA_SIZE 40000
#define THEIR_PORT 2268

int main(int argc, char * argv[]) 
{
    char *file_name = "output.wav";
    int socket_udp, bytes_leidos;
    struct sockaddr_in their_addr;
    struct hostent * he;
   // int sinsize = sizeof(struct sockaddr);
    FILE * fp;
    char buf [DATA_SIZE];
    const char * grabar_iq = "rtl_fm -M iq -f 89.1M -s 170K -g 20 -t 100 -A fast -r 32k - | dd bs=8k count=30 > output.iq";
    const char * crear_wav = "sox -t raw -r 16k -e signed-integer -b 16 -c 2 -V1 output.iq output.wav";

    //Socket UDP
    if((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        return 1;
    }

    //Cargo parametros del cliente
    he = gethostbyname(argv[1]);
    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(THEIR_PORT);
    their_addr.sin_addr = * (struct in_addr *)he->h_addr;
    bzero(&(their_addr.sin_zero),8);

    while(1)
    {
        //Capturo Audio
        system(grabar_iq);
        system(crear_wav);

        //Abrir el archivo de audio
        if ((fp = fopen(file_name, "rb")) == NULL)
        {
            perror("No se pudo abrir el archivo de audio correctamente\n");
            exit(1);
        }

        //Envio de audio
        while ((bytes_leidos = fread(buf, 1, DATA_SIZE, fp)) > 0)
        {
            if ((sendto(socket_udp, buf, bytes_leidos, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1)
            {
                perror("sendto");
                return 1;
            }

            printf("\033[31mEnviando %d bytes\033[0m\n", bytes_leidos);
        }
    
        // Cerrar el archivo de audio
        fclose(fp);
    }

    return 0;
}

