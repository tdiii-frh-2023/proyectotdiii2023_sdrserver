#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <lame/lame.h>

#define PUERTO 10101
#define BUFMAX 1024

void enviar_stream_udp(FILE *archivo, int descriptor_socket) {
    lame_global_flags *gfp = lame_init();
    lame_set_in_samplerate(gfp, 44100);
    lame_set_VBR(gfp, vbr_default);
    lame_init_params(gfp);

    char buffer[BUFMAX];
    size_t bytes_leidos;

    while ((bytes_leidos = fread(buffer, 1, sizeof(buffer), archivo)) > 0) {
        // Codificar el fragmento de audio
        int bytes_codificados = lame_encode_buffer_interleaved(gfp, (short *)buffer, bytes_leidos / 2, buffer, sizeof(buffer));

        // Enviar el fragmento codificado por UDP
        sendto(descriptor_socket, buffer, bytes_codificados, 0, NULL, 0);
    }

    // Enviar el marcador de fin de archivo
    sendto(descriptor_socket, "", 1, 0, NULL, 0);

    lame_close(gfp);
}

int main() {
    // Configurar el cliente UDP
    int descriptor_socket = socket(AF_INET, SOCK_DGRAM, 0);

    if (descriptor_socket == -1) {
        perror("Error al crear el socket");
        exit(EXIT_FAILURE);
    }

    // Configurar la dirección del servidor
    struct sockaddr_in direccion_servidor;
    memset(&direccion_servidor, 0, sizeof(direccion_servidor));
    direccion_servidor.sin_family = AF_INET;
    direccion_servidor.sin_port = htons(PUERTO);
    inet_aton("127.0.0.1", &direccion_servidor.sin_addr);

    // Abrir el archivo MP3 en modo lectura binaria
    FILE *archivo = fopen("archivo.mp3", "rb");

    if (archivo == NULL) {
        perror("Error al abrir el archivo");
        close(descriptor_socket);
        exit(EXIT_FAILURE);
    }

    // Enviar el stream de audio por UDP
    enviar_stream_udp(archivo, descriptor_socket);

    printf("Stream de audio enviado correctamente.\n");

    // Cerrar el socket y el archivo
    close(descriptor_socket);
    fclose(archivo);

    return 0;
}
