/*********************************************
PARA COMPILAR: gcc udp_recv.c -o nombre -lasound

EJECUTAR CON ./nombre localhost (o IP del que envía)
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
extern int h_errno;

#define DATA_SIZE 64000
#define MY_PORT 2268
#define THEIR_PORT 3333

int main() 
{
    int bytes_recibidos;
    snd_pcm_t *pcm_handle;
    snd_pcm_hw_params_t *params;
    unsigned int pcm, sample_rate;
    int socket_udp;
    struct sockaddr_in their_addr, my_addr;
    struct hostent * he;
    int sinsize = sizeof (struct sockaddr);
    char buf[10*DATA_SIZE];


    //Inicializar el dispositivo PCM para reproducción
    if ((snd_pcm_open(&pcm_handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        perror("Error al abrir dispositivo PCM\n");
        exit(1);
    }
    snd_pcm_hw_params_malloc(&params);
    snd_pcm_hw_params_any(pcm_handle, params);


    // Configurar el dispositivo PCM con los parámetros adecuados
    snd_pcm_hw_params_set_access(pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    snd_pcm_hw_params_set_format(pcm_handle, params, SND_PCM_FORMAT_S16_LE);
    snd_pcm_hw_params_set_channels(pcm_handle, params, 2);
    sample_rate = 44100;
    snd_pcm_hw_params_set_rate_near(pcm_handle, params, &sample_rate, 0);

    if ((snd_pcm_hw_params(pcm_handle, params)) < 0)
    {
        perror("No se pudieron establecer los parámetros del dispositivo PCM\n");
        exit(1);
    }

    snd_pcm_hw_params_free(params);


    //Socket UDP
    if((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        return 1;
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MY_PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr.sin_zero),8);

    if((bind(socket_udp, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))) == -1)
    {
        perror("socket");
        return 1;
    }


    //Recibo Audio por UDP
    bytes_recibidos = recvfrom(socket_udp, buf, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
    their_addr.sin_port = htons(THEIR_PORT);
    printf("Reproduciendo audio inicial\n");

    int i;
    while (1)
    {
        i = 0;
        //Recibo Audio por UDP
        for(i=0; i<10; i++)
        {
            bytes_recibidos = recvfrom(socket_udp, buf+(DATA_SIZE*i), DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
            printf("Recibo muestra audio %d\n", i);
        }

        printf("Reproduciendo audio\n");

        //Reproducir Audio
        if ((snd_pcm_writei(pcm_handle, buf, 10*bytes_recibidos / 4)) < 0)
        {
            perror("Error al escribir en el dispositivo PCM\n");
            break;
        }

        //Confirmo que recibi datos
        if ((sendto(socket_udp, &bytes_recibidos, sizeof(int), 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1)
        {
            perror("sendto");
            return 1;
        }

        
    }

    // Cerrar el dispositivo PCM
    snd_pcm_close(pcm_handle);

    return 0;
}

