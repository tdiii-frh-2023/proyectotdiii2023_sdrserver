#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

#define BUF_SIZE 20

//gcc -fPIC -shared -o clibrary.so prueba_ctypes.c para generar el dll

void receive_data(int pipe_fd[])
{
    fd_set pipe_set;
    char buf[BUF_SIZE];

    if(!fork()){
        FD_ZERO(&pipe_set);
        FD_SET(pipe_fd[0], &pipe_set);
        select(pipe_fd[0]+1, &pipe_set, NULL, NULL, NULL); //timeout NULL, select blocks indefinitely
        
        read(pipe_fd[0], buf, BUF_SIZE);

        printf("c process: I received %s \n", buf);
    }
}