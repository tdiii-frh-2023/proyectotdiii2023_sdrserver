#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define conexiones_maximas 5

typedef struct clientes{
    int socket;
    int posicion;
    int estado;
}clientes;

int conexiones_actuales=0;

clientes CLIENTE[conexiones_maximas];

void conectar(int posicion){
    
    CLIENTE[posicion].estado = 1;
    CLIENTE[posicion].posicion = posicion;
    
    conexiones_actuales++;
    
    return;
}

void desconectar(int posicion){
    
    CLIENTE[posicion].estado = 0;
    CLIENTE[posicion].posicion = posicion;
    
    conexiones_actuales--;
    
    return;
}

void buscar_libre(){
    
    int posicion=0;
    
    while( CLIENTE[posicion].estado ){
        posicion++;
        
        if ( !(posicion - conexiones_maximas) ){
            printf("Server lleno\n");            
            return;
        }        
    }
    
    printf("Se encontró la posición %d libre\n",posicion);
    conectar(posicion);
    
    return;
}

void buscar_ocupado(int ingreso){

    if(CLIENTE[ingreso].estado){
        printf("La conexion %d se desconecta\n",ingreso);
        desconectar(ingreso);        
        return;
    }
    
    printf("No está conectado\n");
    
    return;
}

int main(int argc, char **argv) {
    
    int ingreso = 0;
    int posicion;
    while(1){
        scanf("%d",&ingreso);
        printf("\n\n\n\n\n\n\n\n\n\n");
        printf("Ingresó %d\n",ingreso);
        switch(ingreso){
            case 9:
                printf("Entra una conexión\n");
                buscar_libre();
            break;
            default:
                buscar_ocupado(ingreso);

        }
        printf("Conexion\tEstado\n");
        for(int i = 0;i < conexiones_maximas ; i ++){
            printf("%d\t\t%d\n",i,CLIENTE[i].estado);
        }
        printf("\t\tConexiones actuales %d\n",conexiones_actuales);
                
    }
    return 0;
}
