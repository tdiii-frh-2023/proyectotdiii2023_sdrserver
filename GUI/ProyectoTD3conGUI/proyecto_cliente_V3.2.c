/*
Uso: llamar a cliente y mandar como argumento la IP (127.0.0.0)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/shm.h>
#include <signal.h>
#include <sys/msg.h>

#define TCP_PORT 2242
#define DATA_SIZE 64000
#define N 3
#define INITIAL_PORT 55555
#define MSG_SIZE 1000
#define NAME_SIZE 50

int set_tcp (struct sockaddr_in);
int set_udp_port (int);
void choose_name(int);
void receive_udp (int, int);
void handler_int (int val);
void handler_usr1 (int);
void tcp_chat_tx(int, int);
void tcp_chat_rx(int, int);


char * data;
int child_1_id = 0;
int child_2_id = 0;
int new_data = 0;
FILE *vlcPipe;


//gcc -fPIC -shared -o clibrary.so proyecto_cliente_V3.2.c para generar el dll

struct msgbuf{
    long mtype;
    char mdata[MSG_SIZE];
}typedef msgbuf_t;

enum{
    NONE,
    TX_CHAT,
    RX_CHAT,
    UDP_STREAM
}typedef msg_type_t;


//int start(int argc, char * argv[], int msgqid)
int start(int argc, char **argv, int msgqid)
{
    int socket_tcp, socket_udp;
    struct sockaddr_in their_addr_tcp; //Informacion del server
    struct hostent * he; //Direccion a donde se conecta
    int udp_port = 0;
    int pid_client_subprocess;

    printf("Ejecutando proceso cliente (lenguaje c)\n");
    
    
    signal (SIGINT, handler_int);
    signal (SIGUSR2, handler_usr1);

    if(pid_client_subprocess = fork()){
        //La función start debe retornar para que el código de python siga
        //le devuelve el pid del subproceso
        return pid_client_subprocess;
    }

    //Reviso cantidad argumentos
    if (argc != 2)
    {
        perror("argumentos");
        exit(1);
    } 
    
    //Convierto nombre Host a IP
    if ((he = gethostbyname(argv[1]))== NULL)
    {
        perror("gethostbyname");
        exit(1);
    }

    //Server Information
    their_addr_tcp.sin_family = AF_INET;
    their_addr_tcp.sin_port = htons(TCP_PORT);
    their_addr_tcp.sin_addr = *(struct in_addr *)he->h_addr;
    bzero(&(their_addr_tcp.sin_zero), 8);
    
    //Set TCP
    socket_tcp = set_tcp(their_addr_tcp);
    
    //Set UDP port
    socket_udp = set_udp_port(socket_tcp);

    choose_name(socket_tcp);
    
    
    //Send Commands & Chat
    if (!(child_1_id = fork()))
    {
        child_1_id = 0;
        child_2_id = 0;
        tcp_chat_tx(socket_tcp, msgqid);
        exit(0);
    }
    printf("Creado el subproceso envia comandos %d...\n", child_1_id);
    //Receive Msg
    if (!(child_2_id = fork()))
    {
        child_1_id = 0;
        child_2_id = 0;
        tcp_chat_rx(socket_tcp, msgqid);
        exit(0);
    }
    printf("Creado el subproceso recibe cosas %d...\n", child_2_id);

    //Receive and play audio
    close(socket_tcp);
    receive_udp(socket_udp, msgqid);
    
    //Close Sockets
    
    close(socket_udp);

    return 0;
}

int set_tcp (struct sockaddr_in their_addr_tcp)
{
    int socket_tcp;
    
    //Create Socket
    if ((socket_tcp = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket TCP");
        exit(1);
    }
    
    //Connect
    if (connect(socket_tcp, (struct sockaddr *) & their_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("connect TCP");
        exit(1);
    }

    printf ("\e[32mPedido de Conexion hecho\e[0m\n");
    
    return socket_tcp;
} 

int set_udp_port(int socket_tcp)
{
    int udp_port = INITIAL_PORT; //Initial UDP Port
    struct sockaddr_in my_addr_udp;
    int socket_udp;
    
    //Create Socket
    if ((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    
    //Set my_addr_udp
    my_addr_udp.sin_family = AF_INET;
    my_addr_udp.sin_port = htons(udp_port);
    my_addr_udp.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr_udp.sin_zero), 8);
    
    //Bind
    while (bind(socket_udp, (struct sockaddr *)&my_addr_udp, sizeof(struct sockaddr))  == -1)
    {
        udp_port++;
        my_addr_udp.sin_port = htons(udp_port);
    }
    
    //Inform Server
    if (send(socket_tcp, &udp_port, sizeof(int), 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    printf ("\e[34mPuerto UDP: %d\e[0m\n", udp_port);

    return socket_udp;
}

void choose_name (int socket_tcp)
{
    char name [NAME_SIZE];
    size_t len;

    //Ask Name
    printf("Ingrese su nombre: ");
    fgets(name, NAME_SIZE, stdin);

    len = strlen(name);
    if (name[len-1] == '\n')
        name[len-1] = '\0';

    //Inform Server
    if (send(socket_tcp, name, NAME_SIZE, 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    return;
}

void tcp_chat_tx (int socket_tcp, int msgqid)
{
    msgbuf_t msg;

    while(1)
    {
        //Wait for user msg
        bzero(&(msg.mdata), MSG_SIZE);
        msgrcv(msgqid, &msg, sizeof(msgbuf_t), TX_CHAT, 0);
        //printf("C: Tengo que enviar %s\n", msg.mdata);
        //Send via TCP
        if (send(socket_tcp, msg.mdata, MSG_SIZE, 0) == -1)
        {
            perror("send TCP Port");
            exit(1);
        }
    }

    return;
}

void tcp_chat_rx (int socket_tcp, int msgqid)
{
    msgbuf_t msg;

    while(1)
    {
        //Send via TCP
        if (recv(socket_tcp, msg.mdata, MSG_SIZE, 0) == -1)
        {
            perror("send UDP Port");
            exit(1);
        }

        //printf("%s\n", msg);
        //Data is sent to the GUI program
        msg.mtype = RX_CHAT;
        msgsnd(msgqid, &msg, sizeof(msgbuf_t), 0);
    }

    return;
}

void receive_udp(int socket_udp, int msgqid)
{
    char buf[DATA_SIZE*N];
    int i, bytes_acum, bytes_recibidos;
    int sinsize = sizeof(struct sockaddr);
    struct sockaddr_in their_addr;

    vlcPipe = popen("cvlc --quiet -", "w");

    while(1)
    {
        //Reset Variables
        bzero(buf, DATA_SIZE*N);
        i = 0;
        bytes_acum = 0;

        //Receive Audio via UDP
        do
        {
            bytes_recibidos = recvfrom(socket_udp, buf+DATA_SIZE*i, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
            //printf("\033[33mRecibidos %d bytes\033[0m\n", bytes_recibidos);            
            i++;
            bytes_acum += bytes_recibidos;
        }while(bytes_recibidos >= DATA_SIZE);

        //printf("\033[35mTotal paquetes %d\033[0m\n",i);

        //Play Audio
        int escrito = fwrite(buf, sizeof(char), bytes_acum, vlcPipe);
    }
    
    return;
}

void handler_int (int val)
{
    //Kills children & Exits
    if(child_1_id)
        kill(child_1_id, SIGKILL);
    if(child_2_id)
        kill(child_2_id, SIGKILL);
    printf("Matando a %d\n", child_1_id);
    printf("Matando a %d\n", child_2_id);

/*
     if(!child_2_id)
     {
         printf("\033[31mCerrando proceso %d...\033[0m\n", getpid());
         exit(0);
     }
    
        
     if(child_1_id)
         kill(child_1_id, SIGKILL);

    if(child_2_id)
        kill(child_2_id, SIGINT);

    printf("\033[31mCerrando proceso %d...\033[0m\n", getpid());
    printf("\033[31mCerrando proceso %d...\033[0m\n", child_1_id);*/
    exit(0);
}

void handler_usr1 (int val)
{
    new_data = 1;
    return;
}
