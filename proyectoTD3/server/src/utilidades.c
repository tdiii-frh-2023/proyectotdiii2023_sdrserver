#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "utilidades.h"

#define NAME_SIZE 50
#define TCP_PORT 2242
#define MSG_SIZE 1000
#define NAME_SIZE 50
#define BACKLOG 10
#define MAX_CLIENTS 10

void shared_memory_commands (void)
{
    key_t key_sh;
    int shmid;

    //Create Shared Memory
    if ((key_sh = ftok("proyecto_server.c", 'R')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((shmid = shmget (key_sh, MSG_SIZE, 0644 | IPC_CREAT)) == -1)
    {
        perror("shmget");
        exit(1);
    }

    if ((data = shmat(shmid, 0, 0)) == (char *) (-1))
    {
        perror("shmat");
        exit(1);
    }

    return;
}

void shared_memory_msg (void)
{
    key_t key_sh;
    int shmid;

    //Create Shared Memory
    if ((key_sh = ftok("proyecto_server.c", 'Y')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((shmid = shmget (key_sh, MSG_SIZE, 0644 | IPC_CREAT)) == -1)
    {
        perror("shmget");
        exit(1);
    }

    if ((data_msg = shmat(shmid, 0, 0)) == (char *) (-1))
    {
        perror("shmat");
        exit(1);
    }

    return;
}

int get_port(int newfd)
{
    int udp_port;

    if ((recv(newfd, &udp_port, sizeof(int), 0)) == -1)
    {
        perror("Receive Port UDP");
        exit(1);
    }
    printf("\033[34mRecibido Puerto UDP: %d\033[0m\n",udp_port);

    return udp_port;
}

void get_name(int newfd, struct sockaddr_in their_addr_tcp)
{
    int bytes_read;

    if ((bytes_read = recv(newfd, name, NAME_SIZE, 0)) == -1)
    {
        perror("Receive Name");
        exit(1);
    }
    
    if(bytes_read == 0)
    {
        printf("\033[31mConexion perdida desde %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));
        kill(send_tcp_pid, SIGKILL);
        sleep(1);
        exit(0);
    }

    printf("\033[34mNombre del Cliente: %s\033[0m\n", name);

    return;
}

void close_client(int newfd, struct sockaddr_in their_addr_tcp)
{
    printf("\033[31mConexion perdida desde %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));

    //Kill UDP Process
    kill(getppid(), SIGKILL);

    sleep(1);

    //Kill SEND TCP Process
    kill(send_tcp_pid, SIGKILL);

    //Kill RECV TCP Process
    close (newfd);
    exit(0);
}


void handler_usr1 (int val)
{
    int i = 0;

    for(i=0; i<MAX_CLIENTS; i++)
    {
        if(udp_children[i] != 0)
            kill(udp_children[i], SIGUSR2);
    }

    return;
}

void handler_usr2 (int val)
{
    int i;

    new_data = 1;
    change_freq = 1;

    //If Accept Process receives SIGUSR2, sends SIGUSR 2 to every Send TCP Process
    if(getpid() == accept_pid)
    {
        for(i=0; i<MAX_CLIENTS; i++)
        {
            if(send_tcp_children[i] != 0)
                kill(send_tcp_children[i], SIGUSR2);
        }
    }

    return;
}

void handler_child (int val)
{
    wait(0);
    return;
}

void handler_int (int val)
{
    printf("\033[31mCerrando Proceso %d...\033[0m\n", getpid());
    exit(0);
}

void child_id_vector (int udp_new_id, int send_tpc_new_id)
{
    int i = 0;

    //Removes Process no longer alive
    for(i=0; i<MAX_CLIENTS; i++)
    {
        if(udp_children[i] != 0)
        {
            if(kill(udp_children[i], 0)) //Checks if procees is alive
            {
                udp_children[i] = 0;
                send_tcp_children[i] = 0;
            }
        }
    }

    //Puts new PID struct in "children" vector
    i=0;
    while(udp_children[i] != 0)
    {
        i++;
    }

    udp_children[i] = udp_new_id;
    send_tcp_children[i] = send_tpc_new_id;

    //Check how many children alive
    childs = 0;
    for(i=0; i<MAX_CLIENTS; i++)
    {
        if(udp_children[i] != 0)
            childs++;
    }
}
