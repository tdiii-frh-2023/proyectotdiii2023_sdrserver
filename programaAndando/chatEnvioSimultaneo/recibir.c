/*********************************************
PARA COMPILAR: gcc recibir.c -o nombre

EJECUTAR CON ./nombre
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
extern int h_errno;

#define DATA_SIZE 40000
#define MY_PORT 2268
#define THEIR_PORT 3333

int main() 
{
    int bytes_recibidos, i, bytes_acum;
    unsigned int pcm, sample_rate;
    int socket_udp;
    struct sockaddr_in their_addr, my_addr;
    //struct hostent * he;
    int sinsize = sizeof (struct sockaddr);
    char buf[DATA_SIZE*150];
    const char * escuchar = "aplay -f S16_LE -r 24000 -c 1 -"; //Comando Reproduccion
    FILE * aplay;

    
    FILE *vlcPipe = popen("cvlc -", "w");
    
    
    //Socket UDP
    if((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        return 1;
    }

    //Cargo informacion mia
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MY_PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr.sin_zero),8);

    //Bindeo
    if((bind(socket_udp, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))) == -1)
    {
        perror("socket");
        return 1;
    }
        printf("Voy a abrir aplay\n");
        if ((aplay = popen(escuchar, "w")) == NULL) 
        {
            printf("Error al abrir aplay.\n");
            return 1;
        }

    while (1)
    {
        //Limpio Variables
        memset(buf, 0, DATA_SIZE*150);
        i=0;
        bytes_acum = 0;

        //Recibo Audio por UDP
        printf("\033[31mVoy a recibir bytes\033[0m\n");
        do
        {
            bytes_recibidos = recvfrom(socket_udp, buf+DATA_SIZE*i, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
            printf("Recibidos %d bytes \n", bytes_recibidos);
            i++;
            bytes_acum += bytes_recibidos;
        }while(bytes_recibidos >= DATA_SIZE);
        printf("Total paquetes %d\n",i);

        //Reproducir Audio

        // Abrir el proceso aplay en modo escritura


        // Escribir datos al proceso de aplay
        printf("Escrito en buf %d bytes\n",bytes_acum);
        int escrito = fwrite(buf, sizeof(char), bytes_acum, vlcPipe);
        printf("Escribí %d bytes\n",escrito);


    }

    return 0;
}
