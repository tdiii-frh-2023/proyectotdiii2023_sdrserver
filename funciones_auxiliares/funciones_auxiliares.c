#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SDR_TEMPLATE "rtl_fm -M iq -f %s M -s 170K -g 20 -t 100 -A fast -r 32K - | dd bs=8k count=30 output.iq"

int parsear_comando(char *comando_tcp, char *comando_sdr, int size_comando_sdr)
{
    char * token;
    //Asumo que el cliente envía su comando en un string con el siguiente formato:
    //                  /fmradio/<frecuencia en MHz>/
    token = strtok(comando_tcp, "/");

    if(strcmp(token, "fmradio")){
        //Si el comando NO es tipo fmradio retorno -1
        return -1;
    }

    token = strtok(NULL, "/");
    snprintf(comando_sdr, size_comando_sdr, SDR_TEMPLATE, token);
    return 1;
}