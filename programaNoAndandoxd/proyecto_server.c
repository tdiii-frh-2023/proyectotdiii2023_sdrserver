/*
Uso: llamar a server y mandar como argumento la IP de broadcast
Proceso LECTURA: Lee la SDR escribe datos en archivos .wav. Alterna entre 2 archivos.
Proceso Accept: Se queda esperando conexiones nuevas por TCP
Proceso UDP: Envía la información de los archivos por UDP. 
Proceso TCP: Se genera conexión TCP. Acepta cliente y le recibe el puerto UDP. Espera comandos.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>

#define DATA_SIZE 64000
#define BUFFER_SIZE 1000
#define TCP_PORT 2236
#define BACKLOG 10
#define MAX_CLIENTS 10

int set_tcp (void);                 //Creates TCP Socket
void tcp_process (int);             //Accepts TCP
int set_udp (void);                 //Creates TCP Socket
void shared_memory(void);           //Creates Shared Memory
void udp_process (int, struct sockaddr_in); //Accepts TCP
void handler_usr1 (int);
void handler_usr2 (int);
void handler_child (int);
void handler_int (int);
void handler_alarm (int);
void child_id_vector (int);
void parsear_comando(char *);


char buf[64000*15]; //This buffer holds the digitized audio
int new_data = 0;   //1 if children have to read new data in output.wav
int child_id = 0;
int childs = 0;     //Number of children alive
int children [BACKLOG];
char * data;
FILE * pipe_fp;
int mp_id;

int main()
{
    int socket_tcp, socket_udp, newfd;
    struct sockaddr_in their_addr_udp, their_addr_tcp;
    int udp_port, command, numbytes = 1, bytes_read;
    int sin_size = sizeof(struct sockaddr);
    const char * record_iq = "rtl_fm -M iq -f 89.1M -s 170K -g 20 -t 100 -A fast -r 32k - | dd bs=8k count=30 > output.iq";
    const char * create_wav_1 = "sox -t raw -r 16k -e signed-integer -b 16 -c 2 -V1 output.iq output.wav";
    FILE * output_file;

    //Main Process ID
    mp_id = getpid();

    //Capture SIGINT
    signal (SIGINT, handler_int);

    //Create Shared Memory
    shared_memory();

    if (!(child_id = fork()))
    {
        //Capture SIGUSR1 & SIGUSR2
        signal (SIGUSR1, handler_usr1);
        signal (SIGUSR2, handler_usr2);
        signal (SIGCHLD, handler_child);

        //Set TCP
        socket_tcp = set_tcp();
        
        //Set UDP
        socket_udp = set_udp();

        //Clean Children Vector
        bzero(children, BACKLOG);

        while(1)
        {
            //Accept
            if ((newfd = accept(socket_tcp, (struct sockaddr *) &their_addr_tcp, &sin_size)) == -1)
            {
                perror("accept");
                exit(1);
            }
            
            if(childs < MAX_CLIENTS)
            {
                childs++;

                if(!(child_id = fork()))
                {
                    printf("\033[32m\nConexion Establecida con %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));

                    if ((recv(newfd, &udp_port, sizeof(int), 0)) == -1)
                    {
                        perror("Receive Port UDP");
                        exit(1);
                    }
                    printf("\033[34mRecibido Puerto UDP: %d\033[0m\n",udp_port);

                    if(!(child_id = fork()))
                    {
                        //Receive Commands TCP
                        tcp_process(newfd);

                        printf("\033[31mConexion perdida desde %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));
                        kill(getppid(), SIGKILL);
                        close (newfd);
                        exit(0);
                    }       
 
                    //Receive Port
                    their_addr_udp = their_addr_tcp;
                    their_addr_udp.sin_port = htons(udp_port);

                    //Send UDP
                    while(1)
                    {
                        udp_process(socket_udp, their_addr_udp);
                    }
                }
                
                child_id_vector(child_id);
            }
            else
            {
                close(newfd);
                printf("\033[31mConexion Rechazada: Maximo de Clientes Alcanzado\033[0m\n");
            }

        }
        
    }

    signal (SIGALRM, handler_alarm);

    printf("\033[36m------CONFIGURANDO SDR------\033[0m\n");
//    if ((pipe_fp = popen("rtl_fm -M iq -f 89.1M -s 170K -g 20 -A fast -r 32k -", "r")) == NULL)

    if ((pipe_fp = popen("rtl_fm -M wbfm -f 89.1M", "r")) == NULL)
    {
        perror("Error opening pipe");
        return EXIT_FAILURE;
    }

    if ((output_file = fopen("output.iq", "wb")) == NULL)
    {
        perror("Error opening output file");
        pclose(pipe_fp);
        return EXIT_FAILURE;
    }
    
    while(1)
    {
        //Get Audio
        printf("1\n");
        fflush(pipe_fp);
        printf("2\n");
        bzero(buf,DATA_SIZE*15);
        printf("3\n");
        bytes_read = fread(buf, 1, (DATA_SIZE*15),pipe_fp);
        printf("4\n");
//      printf("Escribo archivo (leidos %d bytes)\n",bytes_read);
        rewind(output_file);
        printf("5\n");
        fwrite(buf, 1, bytes_read, output_file);
        printf("6\n");

        //Create .wav File
        system(create_wav_1);
        printf("7\n");
        //New data available
        kill (child_id, SIGUSR1);
        printf("8\n");
    }

    return 0;
}


int set_tcp (void)
{
    int socketfd_tcp;
    struct sockaddr_in my_addr_tcp;
    
    //Create Socket
    if ((socketfd_tcp = socket(AF_INET, SOCK_STREAM ,0)) == -1) 
    {
        perror("socket TCP");
        exit(1);
    }
    
    //set my__addr_tcp
    my_addr_tcp.sin_family = AF_INET;
    my_addr_tcp.sin_port = htons(TCP_PORT);
    my_addr_tcp.sin_addr.s_addr = INADDR_ANY;
    bzero(& (my_addr_tcp.sin_zero), 8);
    
    //Bind Port
    if (bind(socketfd_tcp, (struct sockaddr *) & my_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("bind TCP");
        exit(1);
    }
    
    //Indicate Queue size
    if (listen(socketfd_tcp, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }

    printf("\033[32mTCP Creado en puerto %d...\033[0m\n", TCP_PORT);

    return socketfd_tcp;
}

void shared_memory (void)
{
    key_t key_sh;
    int shmid;

    //Create Shared Memory
    if ((key_sh = ftok("proyecto_server.c", 'R')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((shmid = shmget (key_sh, BUFFER_SIZE, 0644 | IPC_CREAT)) == -1)
    {
        perror("shmget");
        exit(1);
    }

    if ((data = shmat(shmid, 0, 0)) == (char *) (-1))
    {
        perror("shmat");
        exit(1);
    }

    return;
}


void tcp_process (int newfd)
{
    int numbytes = 1;
    char msg [BUFFER_SIZE];

    //Wait for commands
    while(numbytes != 0)
    {
        if ((numbytes = recv(newfd, msg, BUFFER_SIZE, 0)) == -1)
        {
            perror("recv TCP");
            exit(1);
        }
        printf("Mensaje recibido: %s \n", msg);

        parsear_comando(msg);
    }

    return;
}  

int set_udp ()
{
    int socketfd_udp;

    //Create Socket
    if ((socketfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket UDP");
        exit(1);
    }

    return socketfd_udp;
}

void udp_process (int socket_udp, struct sockaddr_in their_addr_udp)
{   
    int i=0, bytes_leidos;
    char * input_file = "output.wav";
    FILE * fd;

    if (new_data == 1)
    {
        new_data = 0;

        //Open file
        if ((fd = fopen(input_file, "rb")) == NULL)
        {
            perror("Open File");
            exit(1);
        }

        //Send audio via UDP
        while ((bytes_leidos = fread(buf, 1, DATA_SIZE, fd)) > 0)
        {
            if ((sendto(socket_udp, buf, bytes_leidos, 0, (struct sockaddr *)&their_addr_udp, sizeof(struct sockaddr))) == -1)
            {
                perror("sendto");
                return;
            }
            usleep(1000);

            //printf("\033[33mEnviando %d bytes al puerto %d\033[0m\n", bytes_leidos, ntohs(their_addr_udp.sin_port));
        }

        //Close file
        fclose(fd);

    }
    
    return;
}

void parsear_comando(char * command)
{
    char * token;
    token = strtok(command, "/");
    char sdr_command[BUFFER_SIZE];
    char * modeloFM = "rtl_fm -M wbfm -f %s";

    //Command Style: /fmradio/<frecuencia en MHz>/
    if(strcmp(token, "fmradio"))
    {
        printf("No es un comando\n");
        return;
    }
    
    bzero(sdr_command,BUFFER_SIZE);
    token = strtok(NULL, "/");
    snprintf(sdr_command, BUFFER_SIZE, modeloFM, token);
    sdr_command[strlen(sdr_command)-1] = ' ';

    printf("El comando recibido es [%s]",sdr_command);
    
    strcpy(data, sdr_command);

    kill(mp_id, SIGALRM);

    return;
}


void handler_usr1 (int val)
{
    int i = 0;

    for(i=0; i<MAX_CLIENTS; i++)
    {
        if(children[i] != 0)
            kill(children[i], SIGUSR2);
    }

    return;
}

void handler_usr2 (int val)
{
    new_data = 1;
    return;
}

void handler_child (int val)
{
    wait(0);
    childs--;
    return;
}

void handler_int (int val)
{
    if (child_id)
        kill(child_id, SIGINT);
    printf("\033[31mCerrando Proceso %d...\033[0m\n", getpid());
    exit(0);
}

void child_id_vector (int new_id)
{
    //Puts new ID in "children" vector
    int i = 0;

    while(children[i] != 0)
    {
        i++;
    }
    children[i] = new_id;
}

void handler_alarm (int val)
{
    //char command[BUFFER_SIZE];

    //strcpy(command, data);
    //printf("Comando: %s\n", data);
    pclose(pipe_fp);
    char * comando_hardcoredsjfekbew = "rtl_fm -M wbfm -f 107.5M";
    if ((pipe_fp = popen(comando_hardcoredsjfekbew, "r")) == NULL)
    {
        perror("Error opening pipe");
        exit(1);
    }
    printf("durmiendo\n");
    sleep(20);
    printf("DESPERAANDTO\n");
    return;

    pclose(pipe_fp);
    if ((pipe_fp = popen(data, "r")) == NULL)
    {
        perror("Error opening pipe");
        exit(1);
    }

    return;
}
