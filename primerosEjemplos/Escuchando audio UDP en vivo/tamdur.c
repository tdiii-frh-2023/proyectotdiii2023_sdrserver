#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define HEADER_SIZE 4
#define MAX_FRAME_SIZE 16384

typedef struct {
    uint32_t sync;
    uint8_t version;
    uint8_t layer;
    uint8_t protection;
    uint8_t bitrate_index;
    uint8_t sampling_rate;
    uint8_t padding;
    uint8_t private;
} MP3Header;

uint32_t read_sync(FILE *file) {
    uint32_t sync;
    fread(&sync, sizeof(uint32_t), 1, file);
    return sync;
}

int read_mp3_header(FILE *file, MP3Header *header) {
    fread(header, sizeof(MP3Header), 1, file);
    return feof(file) ? 0 : 1;
}

double calculate_duration(uint32_t frame_count, uint32_t sampling_rate) {
    return (double)frame_count / sampling_rate;
}

void get_mp3_info(const char *mp3_file, double *duration, long *file_size) {
    FILE *file = fopen(mp3_file, "rb");

    if (!file) {
        perror("Error al abrir el archivo MP3");
        exit(EXIT_FAILURE);
    }

    fseek(file, 0, SEEK_END);
    *file_size = ftell(file);
    rewind(file);

    MP3Header header;
    uint32_t frame_count = 0;

    while (read_sync(file) == 0xFFE00000) {
        if (read_mp3_header(file, &header) == 0) {
            break;
        }

        // Calcula la duración solo si el bitrate y la frecuencia de muestreo son válidos
        if (header.bitrate_index != 0x00 && header.bitrate_index != 0x0F &&
            header.sampling_rate != 0x03) {
            frame_count++;
        }

        fseek(file, header.padding, SEEK_CUR);
    }

    *duration = calculate_duration(frame_count, 44100); // Asumiendo frecuencia de muestreo de 44.1 kHz

    fclose(file);
}

int main() {
    const char *mp3_file_path = "enviar.mp3";

    double duration;
    long file_size;

    get_mp3_info(mp3_file_path, &duration, &file_size);

    printf("Duración del MP3: %.2f segundos\n", duration);
    printf("Tamaño del archivo: %ld bytes\n", file_size);

    return 0;
}
