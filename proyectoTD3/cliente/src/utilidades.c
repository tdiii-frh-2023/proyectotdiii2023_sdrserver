#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include "utilidades.h"

#define NAME_SIZE 50

void choose_name (int socket_tcp)
{
    char name [NAME_SIZE];
    size_t len;

    //Ask Name
    printf("Ingrese su nombre: ");
    fgets(name, NAME_SIZE, stdin);

    len = strlen(name);
    if (name[len-1] == '\n')
        name[len-1] = '\0';

    //Inform Server
    if (send(socket_tcp, name, NAME_SIZE, 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    return;
}

void handler_int (int val)
{
    printf("\033[31mCerrando proceso %d...\033[0m\n", getpid());
    exit(0);
}
