/*********************************************
PARA COMPILAR: gcc udp_recv_mic.c -o nombre -lasound

EJECUTAR CON ./nombre

ESTE PROGRAMA LEE MIC Y LO ENVIA POR UDP
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <signal.h>

//extern int h_errno;

#define DATA_SIZE 128
#define MY_PORT 2268

void handlerkill(int val);

int socket_udp;
snd_pcm_t * pcm_handle;

int main() 
{
    int bytes_recibidos;
    snd_pcm_hw_params_t * params;
    unsigned int sample_rate;
    struct sockaddr_in their_addr, my_addr;
    int sinsize = sizeof (struct sockaddr);
    char buf[DATA_SIZE];
    int dir;
    snd_pcm_uframes_t frames;

    signal(SIGINT, handlerkill);

    //Inicializar el dispositivo PCM para reproduccion
    if ((snd_pcm_open(&pcm_handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        perror("Error al abrir dispositivo PCM\n");
        exit(1);
    }

    // Configurar el dispositivo PCM con los parametros adecuados
    snd_pcm_hw_params_malloc(&params);
    snd_pcm_hw_params_any(pcm_handle, params);
    snd_pcm_hw_params_set_access(pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    snd_pcm_hw_params_set_format(pcm_handle, params, SND_PCM_FORMAT_S16_LE);
    snd_pcm_hw_params_set_channels(pcm_handle, params, 2);
    sample_rate = 44100;
    snd_pcm_hw_params_set_rate_near(pcm_handle, params, &sample_rate, &dir);
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(pcm_handle, params, &frames, &dir);

    //Aplicar los parametros
    if ((snd_pcm_hw_params(pcm_handle, params)) < 0)
    {
        perror("No se pudieron establecer los parametros del dispositivo PCM\n");
        exit(1);
    }
    snd_pcm_hw_params_free(params);


    //Socket UDP
    if((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        return 1;
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MY_PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr.sin_zero),8);

    if((bind(socket_udp, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))) == -1)
    {
        perror("socket");
        return 1;
    }

    while (1)
    {
        //Recibo Audio por UDP
        bytes_recibidos = recvfrom(socket_udp, buf, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);

        //Reproducir Audio
        if ((snd_pcm_writei(pcm_handle, buf, bytes_recibidos/4)) < 0)
        {
            perror("Error al escribir en el dispositivo PCM\n");
            break;
        }
    }

    return 0;
}

void handlerkill(int val)
{
    printf("Saliendo del programa\n");
    snd_pcm_close(pcm_handle);
    close(socket_udp);
    exit(0);
}
