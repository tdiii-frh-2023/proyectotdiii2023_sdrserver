#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include "cliente_UDP.h"

#define N 3
#define DATA_SIZE 64000
#define INITIAL_PORT 55555

int set_udp_port(int socket_tcp)
{
    int udp_port = INITIAL_PORT; //Initial UDP Port
    struct sockaddr_in my_addr_udp;
    int socket_udp;
    
    //Create Socket
    if ((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    
    //Set my_addr_udp
    my_addr_udp.sin_family = AF_INET;
    my_addr_udp.sin_port = htons(udp_port);
    my_addr_udp.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr_udp.sin_zero), 8);
    
    //Bind
    while (bind(socket_udp, (struct sockaddr *)&my_addr_udp, sizeof(struct sockaddr))  == -1)
    {
        udp_port++;
        my_addr_udp.sin_port = htons(udp_port);
    }
    
    //Inform Server
    if (send(socket_tcp, &udp_port, sizeof(int), 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    printf ("\e[34mPuerto UDP: %d\e[0m\n", udp_port);

    return socket_udp;
}

void receive_udp(int socket_udp)
{
    char buf[DATA_SIZE*N];
    int i, bytes_acum, bytes_recibidos;
    int sinsize = sizeof(struct sockaddr);
    struct sockaddr_in their_addr;

    FILE *vlcPipe = popen("cvlc --quiet -", "w");

    while(1)
    {
        //Reset Variables
        bzero(buf, DATA_SIZE*N);
        i = 0;
        bytes_acum = 0;

        //Receive Audio via UDP
        do
        {
            bytes_recibidos = recvfrom(socket_udp, buf+DATA_SIZE*i, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
            i++;
            bytes_acum += bytes_recibidos;
        }while(bytes_recibidos >= DATA_SIZE);

        //Play Audio
        int escrito = fwrite(buf, sizeof(char), bytes_acum, vlcPipe);
    }
    
    return;
}
