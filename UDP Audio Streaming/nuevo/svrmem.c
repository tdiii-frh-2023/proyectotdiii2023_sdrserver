#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#define PUERTO_TCP (int)12121
#define BUFFER_TAM (int)32768
#define COLATAM (int)8

char buffer[BUFFER_TAM];

FILE * archivo;


int main(void){
    
    int socket_TCP;
    int socket_UDP;
    
    int sin_tam = sizeof(struct sockaddr);

    struct sockaddr_in direccion_servidor_TCP, direccion_cliente_TCP;
    struct sockaddr_in direccion_servidor_UDP, direccion_cliente_UDP;
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    int reuseaddr = 1;
    int error;
    // setsockopt permite reutilizar varias veces el mismo BIND
    error = setsockopt(
        socket_TCP,
        SOL_SOCKET,
        SO_REUSEADDR,
        &reuseaddr,
        sizeof(int)
            );
    
    if ( error == -1) {
        perror("setsockopt");
        exit(1);
    }
    
    direccion_servidor_TCP.sin_family = AF_INET;
    direccion_servidor_TCP.sin_port = htons(PUERTO_TCP);
    direccion_servidor_TCP.sin_addr.s_addr = INADDR_ANY;
    bzero(&(direccion_servidor_TCP.sin_zero),8);
    
    printf("Preparando bind\n");
    
    error = bind(
        socket_TCP,
         (struct sockaddr *)    &direccion_servidor_TCP,
         sin_tam
    );
    
    if (error == -1){
        printf("bind\n\n");
        exit(1);
    }
    
    printf("Preparando listen\n");
    
    listen(
        socket_TCP,
        10
    );

    printf("Preparando accept\n");
    
    int socket_TCP_cliente;
    
    socket_TCP_cliente = accept(
        socket_TCP,
        (struct sockaddr *) &direccion_cliente_TCP,
        &sin_tam
                            );
    
    const char * escuchaMandar= "rtl_fm -f 101.1M -M wbfm -s 200000 -r 24000 - | sox -traw -r24k -es -b16 -c1 -V1 - -tmp3 - | socat -u - TCP-LISTEN:12121";
    
    int return_code = system(escuchaMandar);
    
    while(1);
    
    
    return 0;
}
