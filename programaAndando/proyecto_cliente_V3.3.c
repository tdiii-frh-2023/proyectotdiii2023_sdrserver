/*
Uso: llamar a cliente y mandar como argumento la IP (127.0.0.0)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/shm.h>
#include <signal.h>

#define TCP_PORT 2242
#define DATA_SIZE 64000
#define N 3
#define INITIAL_PORT 55555
#define MSG_SIZE 1000
#define NAME_SIZE 50

/****************************************************
Descripción: Crea el Socket TCP y Pide conexión al Server
Argumento: Direccion del Server
Retorna: Socket TCP
*****************************************************/
int set_tcp (struct sockaddr_in);

/***************************************************************************
Descripción: Crea el Socket UDP, busca puerto disponible y lo manda por TCP
Argumento: Socket TCP
Retorna: Socket UDP
***************************************************************************/
int set_udp_port (int);

/****************************************************
Descripción: Pide nombre a usuario y lo envia por TCP
Argumento: Socket TCP
Retorna: Nada
*****************************************************/
void choose_name(int);

/****************************************************
Descripción: Recibe audio por UDP y lo reproduce
Argumento: Socket UDP
Retorna: Nada
*****************************************************/
void receive_udp (int);

/****************************************************
Descripción: Handler Señal SIGINT. Termina proceso
Argumento: No Utilizado
Retorna: Nada
*****************************************************/
void handler_int (int);

/********************************************************
Descripción: Envia por TCP mensajes que escribe el usuario
Argumento: Socket TCP
Retorna: Nada
*********************************************************/
void tcp_chat_tx(int);

/********************************************************
Descripción: Recibe y muestra en pantalla mensajes por TCP
Argumento: Socket TCP
Retorna: Nada
*********************************************************/
void tcp_chat_rx(int);


char * data;
int child_1_id = 0;
int child_2_id = 0;

int main (int argc, char * argv[])
{
    int socket_tcp, socket_udp;
    struct sockaddr_in their_addr_tcp; //Informacion del server
    struct hostent * he; //Direccion a donde se conecta
    int udp_port = 0;

    signal (SIGINT, handler_int);

    //Reviso cantidad argumentos
    if (argc != 2)
    {
        perror("argumentos");
        exit(1);
    } 
    
    //Convierto nombre Host a IP
    if ((he = gethostbyname(argv[1]))== NULL)
    {
        perror("gethostbyname");
        exit(1);
    }
    
    //Server Information
    their_addr_tcp.sin_family = AF_INET;
    their_addr_tcp.sin_port = htons(TCP_PORT);
    their_addr_tcp.sin_addr = *(struct in_addr *)he->h_addr;
    bzero(&(their_addr_tcp.sin_zero), 8);
    
    //Set TCP
    socket_tcp = set_tcp(their_addr_tcp);
    
    //Set UDP port
    socket_udp = set_udp_port(socket_tcp);

    choose_name(socket_tcp);
    
    //Send Commands & Chat
    if (!(child_1_id = fork()))
    {
        tcp_chat_tx(socket_tcp);
        exit(0);
    }

    //Receive Msg
    if (!(child_2_id = fork()))
    {
        tcp_chat_rx(socket_tcp);
        exit(0);
    }

    //Receive and play audio
    receive_udp(socket_udp);
    
    //Close Sockets
    close(socket_tcp);
    close(socket_udp);

    return 0;
}

int set_tcp (struct sockaddr_in their_addr_tcp)
{
    int socket_tcp;
    
    //Create Socket
    if ((socket_tcp = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket TCP");
        exit(1);
    }
    
    //Connect
    if (connect(socket_tcp, (struct sockaddr *) & their_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("connect TCP");
        exit(1);
    }

    printf ("\e[32mPedido de Conexion hecho\e[0m\n");
    
    return socket_tcp;
} 

int set_udp_port(int socket_tcp)
{
    int udp_port = INITIAL_PORT; //Initial UDP Port
    struct sockaddr_in my_addr_udp;
    int socket_udp;
    
    //Create Socket
    if ((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    
    //Set my_addr_udp
    my_addr_udp.sin_family = AF_INET;
    my_addr_udp.sin_port = htons(udp_port);
    my_addr_udp.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr_udp.sin_zero), 8);
    
    //Bind
    while (bind(socket_udp, (struct sockaddr *)&my_addr_udp, sizeof(struct sockaddr))  == -1)
    {
        udp_port++;
        my_addr_udp.sin_port = htons(udp_port);
    }
    
    //Inform Server
    if (send(socket_tcp, &udp_port, sizeof(int), 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    printf ("\e[34mPuerto UDP: %d\e[0m\n", udp_port);

    return socket_udp;
}

void choose_name (int socket_tcp)
{
    char name [NAME_SIZE];
    size_t len;

    //Ask Name
    printf("Ingrese su nombre: ");
    fgets(name, NAME_SIZE, stdin);

    len = strlen(name);
    if (name[len-1] == '\n')
        name[len-1] = '\0';

    //Inform Server
    if (send(socket_tcp, name, NAME_SIZE, 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    return;
}

void tcp_chat_tx (int socket_tcp)
{
    char msg [MSG_SIZE];
    while(1)
    {
        //Wait for user msg
        fgets(msg, MSG_SIZE, stdin);

        //Send via TCP
        if (send(socket_tcp, msg, MSG_SIZE, 0) == -1)
        {
            perror("send UDP Port");
            exit(1);
        }
    }

    return;
}

void tcp_chat_rx (int socket_tcp)
{
    char msg [MSG_SIZE];
    int numbytes = 0;
    
    while(1)
    {
        //Send via TCP
        if ((numbytes = recv(socket_tcp, msg, MSG_SIZE, 0)) == -1)
        {
            perror("send UDP Port");
            exit(1);
        }
        
        if(numbytes != 0)
            printf("%s\n", msg);
    }

    return;
}

void receive_udp(int socket_udp)
{
    char buf[DATA_SIZE*N];
    int i, bytes_acum, bytes_recibidos;
    int sinsize = sizeof(struct sockaddr);
    struct sockaddr_in their_addr;

    FILE *vlcPipe = popen("cvlc --quiet -", "w");

    while(1)
    {
        //Reset Variables
        bzero(buf, DATA_SIZE*N);
        i = 0;
        bytes_acum = 0;

        //Receive Audio via UDP
        do
        {
            bytes_recibidos = recvfrom(socket_udp, buf+DATA_SIZE*i, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
            i++;
            bytes_acum += bytes_recibidos;
        }while(bytes_recibidos >= DATA_SIZE);

        //Play Audio
        int escrito = fwrite(buf, sizeof(char), bytes_acum, vlcPipe);
    }
    
    return;
}

void handler_int (int val)
{
    printf("\033[31mCerrando proceso %d...\033[0m\n", getpid());
    exit(0);
}