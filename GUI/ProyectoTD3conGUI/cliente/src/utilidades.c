#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include "utilidades.h"
#include <sys/signal.h>

#define NAME_SIZE 50

extern int child_1_id;
extern int child_2_id;

void choose_name (int socket_tcp, char *name)
{
    size_t len;

    len = strlen(name);
    if (name[len-1] == '\n')
        name[len-1] = '\0';

    //Inform Server
    if (send(socket_tcp, name, NAME_SIZE, 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    return;
}

void handler_int (int val)
{
    //Kills children & Exits
    if(child_1_id)
        kill(child_1_id, SIGKILL);
    if(child_2_id)
        kill(child_2_id, SIGKILL);
    printf("\033[31mCerrando proceso %d...\033[0m\n", child_1_id);
    printf("\033[31mCerrando proceso %d...\033[0m\n", child_2_id);
    printf("\033[31mCerrando proceso %d...\033[0m\n", getpid());

    exit(0);
}