#ifndef CLIENTE_TCP_H
#define CLIENTE_TCP_H

/****************************************************
Descripción: Crea el Socket TCP y Pide conexión al Server
Argumento: Direccion del Server
Retorna: Socket TCP
*****************************************************/
int set_tcp (struct sockaddr_in);

/********************************************************
Descripción: Envia por TCP mensajes que escribe el usuario
Argumento: Socket TCP
Retorna: Nada
*********************************************************/
void tcp_chat_tx(int);

/********************************************************
Descripción: Recibe y muestra en pantalla mensajes por TCP
Argumento: Socket TCP
Retorna: Nada
*********************************************************/
void tcp_chat_rx(int);

#endif
