/*********************************************
PARA COMPILAR: gcc Audio.c -o nombre -lasound

EL PROGRAMA FUNCIONA CON ARCHIVOS .wav Y .raw (NO .mp3)
*********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>

#define DATA_SIZE 40000

int main() 
{
    snd_pcm_t * pcm_handle;
    snd_pcm_hw_params_t * params;
    unsigned int pcm, sample_rate;
    char * file_name = "audio.wav";
    FILE * fp;
    char buffer[DATA_SIZE];
    int bytes_read;

    //Abrir el archivo de audio
    if ((fp = fopen(file_name, "rb")) == NULL)
    {
        perror("No se pudo abrir el archivo de audio correctamente\n");
        exit(1);
    }

    //Inicializar el dispositivo PCM para reproducción
    if ((snd_pcm_open(&pcm_handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        perror("Error al abrir dispositivo PCM\n");
        exit(1);
    }
    snd_pcm_hw_params_malloc(&params);
    snd_pcm_hw_params_any(pcm_handle, params);

    // Configurar el dispositivo PCM con los parámetros adecuados
    snd_pcm_hw_params_set_access(pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    snd_pcm_hw_params_set_format(pcm_handle, params, SND_PCM_FORMAT_S16_LE);
    snd_pcm_hw_params_set_channels(pcm_handle, params, 2);
    sample_rate = 44100;
    snd_pcm_hw_params_set_rate_near(pcm_handle, params, &sample_rate, 0);

    if ((snd_pcm_hw_params(pcm_handle, params)) < 0)
    {
        perror("No se pudieron establecer los parámetros del dispositivo PCM\n");
        exit(1);
    }

    snd_pcm_hw_params_free(params);


    //Se puede mover la ventana para no comenzar desde el principio
    //fseek(fp, 50000000, SEEK_SET);


    //Leer archivo y reproducir el audio
    while ((bytes_read = fread(buffer, 1, DATA_SIZE, fp)) > 0)
    {
        if ((snd_pcm_writei(pcm_handle, buffer, bytes_read / 4)) < 0)
        {
            perror("Error al escribir en el dispositivo PCM\n");
            break;
        }
    }


    //Final de la reproducción
    if (bytes_read == 0)
        snd_pcm_drain(pcm_handle); //Termino archivo
    else
        fprintf(stderr, "Error al leer el archivo de audio\n"); //Error previo

    //Cerrar el archivo y el dispositivo PCM
    fclose(fp);
    snd_pcm_close(pcm_handle);

    return 0;
}

