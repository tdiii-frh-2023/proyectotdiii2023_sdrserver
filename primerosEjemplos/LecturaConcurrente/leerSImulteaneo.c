#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <pthread.h>

const char * archivoNombre = "enviar.txt";
int archivoTam = 100;
int bytes_escritos = 10;

int posicionLectura = 0;
int posicionEscritura = 0;

int leido = 1;
int escrito = 1;

void * escribirHilo(void * arg);
void * leerHilo(void * arg);

int main(void){
    
    FILE * archivo;
    
    archivo = fopen(archivoNombre,"wb");
    
    char datos[archivoTam];
    for ( int i = 0 ; i < archivoTam ; i ++ ){
        datos[i] = i % 26 + 'A';
    }
    
    size_t bytes_escritos = fwrite(datos,
                                   1,
                                   archivoTam,
                                   archivo);
    
    fclose(archivo);
    
    pthread_t escritura,lectura;
    
    pthread_create( &escritura,
                    NULL,
                    escribirHilo,
                    NULL
    );
    
    while(posicionEscritura<50) leido = 1;;
    
    pthread_create( &lectura,
                    NULL,
                    leerHilo,
                    NULL
    );
    
    pthread_join(   escritura,
                    NULL
    );
    
    pthread_join(   lectura,
                    NULL
    );
    
    
    return 0;
}

void * escribirHilo(void * arg){
    
    FILE * archivo;
    
    archivo = fopen(archivoNombre,"wb");
    
    char datos[bytes_escritos];
    
    for ( int i = 0 ; i < bytes_escritos ; i ++ ){
        datos[i] = i % 26 + 'A';
    }
    
    while ( posicionEscritura < archivoTam ){
        if (leido){
            leido = 0;
            fseek(archivo,posicionEscritura,SEEK_SET);
            fwrite( datos,
                    1,
                    bytes_escritos,
                    archivo
            );
            escrito = 1;
            datos[bytes_escritos] = '\0';
            printf("Escritos %d bytes ( %s ) en la posicion %d\n",strlen(datos),datos,posicionEscritura);
            posicionEscritura += bytes_escritos;
        }
    }    
    fclose(archivo);
    
    return NULL;
}

void * leerHilo(void * arg){
    
    FILE * archivo;
    
    archivo = fopen(archivoNombre,"rb");
    
    char datos[bytes_escritos-1];
    
    while ( posicionLectura < archivoTam ){
        if(escrito){
            escrito = 0;
            fseek(archivo,posicionLectura,SEEK_SET);
            fread(  datos,
                    1,
                    bytes_escritos,
                    archivo
            );
            leido = 1;
            printf("Leidos %d bytes ( %s ) desde la posicion %d\n",strlen(datos),datos,posicionLectura);
            posicionLectura += bytes_escritos;
        }
    }    
    fclose(archivo);
    
    return NULL;
}

