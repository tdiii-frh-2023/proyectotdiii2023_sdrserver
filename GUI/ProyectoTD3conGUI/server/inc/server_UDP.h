#ifndef SERVER_UDP_H
#define SERVER_UDP_H

extern int new_data;   //1 if children have to read new data in output.wav

/****************************************************
Descripción: Crea Socket UDP
Argumento: Ninguno
Retorna: Socket UDP
*****************************************************/
int set_udp (void);

/****************************************************
Descripción: Lee Wav y lo envia por UDP al cliente asociado
Argumento: Socket UDP y Direccion UDP del cliente
Retorna: Nada
*****************************************************/
void udp_process (int, struct sockaddr_in);

#endif
