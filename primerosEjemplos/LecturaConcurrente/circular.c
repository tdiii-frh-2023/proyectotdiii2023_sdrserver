#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <pthread.h>
#include <semaphore.h>

const char * archivoNombre = "enviar.txt";
int archivoTam = 26;
int bytes_escritos = 1;

int posicionAdeltantado = 0;
int posicionAtrasado = 0;

sem_t hilo1,hilo2;

void * adeltantado(void * arg);
void * atradaso(void * arg);
int calcularPosicion(int );


int main(void){
    
    FILE * archivo;
    
    archivo = fopen(archivoNombre,"wb");
    
    char datos[archivoTam];
    for ( int i = 0 ; i < archivoTam ; i ++ ){
        datos[i] = i % 26 + 'A';
    }
    
    size_t bytes_escritos = fwrite( datos,
                                    1,
                                    archivoTam,
                                    archivo
                            );
    
    fclose(archivo);
    
    sem_init(&hilo1, 0, 1);
    sem_init(&hilo2, 0, 1);
    
    sem_post(&hilo2);

    
    char colaCircular[archivoTam];
    
    pthread_t adeltantado_id,atradaso_id;
    
    pthread_create( &adeltantado_id,
                    NULL,
                    adeltantado,
                    NULL
    );
    
    pthread_create( &atradaso_id,
                    NULL,
                    atradaso,
                    NULL
    );
    
    pthread_join(   adeltantado_id,
                    NULL
    );
    
    pthread_join(   atradaso_id,
                    NULL
    );
    
    
    return 0;
}

void * adeltantado(void * arg){
    
    FILE * archivo = fopen(archivoNombre,"rb");
    char dato[2];
    
    while(1){
        sem_wait(&hilo2);
        fseek(  archivo,
                posicionAdeltantado,
                SEEK_SET
        );
        fread(  dato,
                1,
                sizeof(dato),
                archivo
        );
        sem_post(&hilo1);
        dato[1] = '\0';
        printf("El adeltantado leyó el dato \t%s en %d\n",dato,posicionAdeltantado);
        posicionAdeltantado = calcularPosicion(posicionAdeltantado);
        fgets(dato,2,stdin);
        
    }
    
    return NULL;
}

void * atradaso(void * arg){
    FILE * archivo = fopen(archivoNombre,"rb");
    char dato[2];
    
    posicionAtrasado = calcularPosicion(posicionAdeltantado+5);
    
    while(1){
        sem_wait(&hilo1);
        fseek(  archivo,
                posicionAtrasado,
                SEEK_SET
        );
        fread(  dato,
                1,
                sizeof(dato),
                archivo
        );
        sem_post(&hilo2);
        dato[1] = '\0';
        printf("El atrasado leyó el dato \t%s en %d\n",dato,posicionAtrasado);
        posicionAtrasado = calcularPosicion(posicionAdeltantado+5);
    }
    
    return NULL;
}

int calcularPosicion(int posicion){
    if (posicion > (archivoTam-2))  return posicion - archivoTam + 1;
    
    return  ++posicion;
}

