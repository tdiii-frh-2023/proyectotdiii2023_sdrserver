import sys
import os
import ctypes
from ctypes import c_int
import time
################con este modulo los datos no se reciben bien################
from ipcqueue import sysvmq

# def main():
    
#     clibrary = ctypes.CDLL("/home/tux/Desktop/proyectotdiii2023_sdrserver/GUI/clibrary.so")
    
#     q = sysvmq.Queue(0) #key = 0, private queue
#     q_c = c_int(q._queue_id)
#     clibrary.receive_data(q_c)
#     time.sleep(2)
#     q.put(b"hola", msg_type=1)
#     time.sleep(2)
#     q.put(b'desde', msg_type=2)
#     time.sleep(2)
#     q.put(b'Python', msg_type=3)
#     exit()

################con este modulo los datos se reciben bien################
import sysv_ipc

def main():
    
    clibrary = ctypes.CDLL("/home/tux/Desktop/proyectotdiii2023_sdrserver/GUI/clibrary.so")
    
    q = sysv_ipc.MessageQueue(sysv_ipc.IPC_PRIVATE, sysv_ipc.IPC_CREAT | sysv_ipc.IPC_EXCL)
    q_c = c_int(q.id)
    clibrary.receive_data(q_c)
    time.sleep(1)
    message = "hola"
    b_message = message.encode('utf-8')
    q.send(b_message, type=2)
    time.sleep(1)
    q.send(b"desde", type=2)
    time.sleep(1)
    q.send(b"Python", type=3)
    #q.remove() #si elimino la cola surge un error que hace referencia a una linea previa (35), raro
    exit()

if __name__=="__main__":
    main()

