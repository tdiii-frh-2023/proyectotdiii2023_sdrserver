#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#define PUERTO_TCP (int)10103
#define PUERTO_UDP (int)21012
#define BUFFER_TAM (int)1024
#define DATA_SIZE (int)21012
#define CLIENTES_MAX (int)5

#define rojo "\033[31m"
#define verde "\033[32m"
#define amarillo "\033[33m"
#define magentaclaro "\033[91m"
#define magenta "\033[35m"
#define ciano "\033[36m"
#define negro "\033[0m"

int socket_TCP;
int socket_UDP;

int sin_tam = sizeof(struct sockaddr);

struct sockaddr_in direccion_servidor_TCP, direccion_cliente_TCP;
struct sockaddr_in direccion_servidor_UDP, direccion_cliente_UDP;
char * modeloFM = "rtl_fm -M wbfm -f %s";
const char * crear_wav = "sox -t raw -r 16k -e signed-integer -b 16 -c 2 -V1 output.iq output.wav";

FILE * fp;
char *file_name = "output.wav";
int bytes_leidos;

int posx;

char * colores[] = {
    "\033[32m",
    "\033[33m",
    "\033[91m",
    "\033[35m",
    "\033[36m",
    "\033[31m"
};

FILE * pipe_fp;
FILE * output_file;
char buffer2[DATA_SIZE*15];
char buf[DATA_SIZE*15];


char * buffer;

int conexionesActuales = 0;

struct sembuf sb = {0 ,0, 0 };

typedef struct clientes{
    int socket;
    int estado; //0 libre, 1 ocupado
    int posicion;
    char usuario[32];
    in_addr_t direccion;
}clientes;

//struct clientes CLIENTE[CLIENTES_MAX];
struct clientes * datos;

struct clientes SV = {-1,-1,CLIENTES_MAX,"Server"};

int shmid;
int shmbufid;

void semaforos_pedir(int id, int cantidad ){
    sb.sem_op = cantidad;
    semop(id, &sb, 1);
}

void semaforos_dar(int id, int cantidad ){
    sb.sem_op = cantidad;
    semop(id, &sb, 1);
}

void mostrarConexiones(void){
    
    printf("Cliente\tEstado\n");
    for( int i = 0 ; i < CLIENTES_MAX ; i ++ ){
        printf("%d\t%d en el socket %d\n",i,(datos+i)->estado,(datos+i)->socket);
    }
    
    return;
}

int tomarHilo(int socket, in_addr_t direccion){
    
    int i = 0;
    while( (datos+i)->estado ){
        i++;
    }
    
    //printf("Posicion libre encontrada en %d\n",i);
    (datos+i)->estado = 1;
    (datos+i)->socket = socket;
    (datos+i)->direccion = direccion;

    
    conexionesActuales++;
    
    return i;
}

void limpiarHilo(int socket){
    
    int i = 0;
    while( socket -(datos+i)->socket ){
        i++;
    }
    
    //printf("Liberado %d\n",i);
    
    (datos+i)->estado = 0;
    (datos+i)->socket = 0;
    
    conexionesActuales--;
    
    return;
}

void reenviarInformacion(char * buffer, clientes CLIENTE){
    
    int i = 0;
    
    char reenviar[BUFFER_TAM];
    
    for ( i = 0 ; i < CLIENTES_MAX ; i ++ ){
        if ( (datos+i)->posicion - CLIENTE.posicion ){
            if ( (datos+i)->estado ){
                bzero(reenviar,BUFFER_TAM);
                strcat(reenviar,colores[CLIENTE.posicion]);
                strcat(reenviar,CLIENTE.usuario);
                strcat(reenviar,negro);
                strcat(reenviar,buffer);
                send(
                    (datos+i)->socket,
                    reenviar,
                    strlen(reenviar),
                    0
                );
                //printf("Enviado mensaje %s al socket %d\n",reenviar,(datos+i)->socket);
            }
        }
    }
    
    return;
}

void banear(char * buffer){
    for ( int i = 0 ; i < CLIENTES_MAX ; i ++ ){
        printf("COMPARANDO %s con %s\n",buffer,(datos+i)->usuario);
        if ( ! strcmp(buffer,(datos+i)->usuario ) ){
            close((datos+i)->socket);
        }
    }
}


void * chatHilo(void * mensaje){
    
    while(1){
        printf("Enviar mensaje a todos los clientes\n");
        fgets(buffer,BUFFER_TAM,stdin);
        banear(buffer);
        reenviarInformacion(buffer,SV);
    }
    
    return NULL;
}

void chatHiloCrear(){
    
    pthread_t thread_id;
    pthread_create(
        &thread_id,
        NULL,
        chatHilo,
        &thread_id
    );

    
    return;
}

void * streamHilo(void * mensaje){

    socket_UDP = socket(
        AF_INET,
        SOCK_DGRAM,
        0
                    );
    direccion_servidor_UDP.sin_family = AF_INET;
    direccion_servidor_UDP.sin_port = htons(PUERTO_UDP);
    direccion_servidor_UDP.sin_addr.s_addr = INADDR_ANY;
    bzero(&(direccion_servidor_UDP.sin_zero),8);
    printf("Listo para enviar UDP\n");
    int i = 0;
    int leer;
    sleep(10);
    while(1){
        bzero(buffer2,DATA_SIZE*15);
        bzero(buf,DATA_SIZE*15);
            //Capturo Audio
            fflush(pipe_fp);
            bzero(buffer2,DATA_SIZE*15);
            int leido = fread(buffer2, 1, DATA_SIZE*15,pipe_fp);
            //printf("Escribo archivo (leidos %d bytes)\n",leido);
            rewind(output_file);
            int escrito = fwrite(buffer2, 1, leido, output_file);
            //printf("Creo wav (escritos %d bytes)\n",escrito);
            
            system(crear_wav);

            //Abrir el archivo de audio
            if ((fp = fopen(file_name, "rb")) == NULL)
            {
                perror("No se pudo abrir el archivo de audio correctamente\n");
                exit(1);
            }

            //Envio de audio
            int cont=0;
            for(i=0;i<CLIENTES_MAX;i++){
                rewind(fp);
                cont = 0;
                bzero(buf,DATA_SIZE*15);
            direccion_servidor_UDP.sin_port = htons(PUERTO_UDP+i);
            {
            while ((bytes_leidos = fread(buf, 1, DATA_SIZE, fp)) > 0)
            {
                if ((sendto(socket_UDP, buf, bytes_leidos, 0, (struct sockaddr *)&direccion_servidor_UDP, sizeof(struct sockaddr))) == -1)
                {
                    perror("sendto");
                }
                cont++;
                //printf("\033[31mEnviando %d bytes\033[0m\n", bytes_leidos);
                usleep(1000);
            }
            }
            //printf("Total paquetes %d a %d\n",cont,PUERTO_UDP+i);
            //printf("Tamaño archivo wav %ld bytes\n",ftell(fp));
        
            // Cerrar el archivo de audio
            
        }fclose(fp);
    }    
    return NULL;
}

void streamHiloCrear(){
    
    pthread_t thread_id;
    pthread_create(
        &thread_id,
        NULL,
        streamHilo,
        &thread_id
    );

    
    return;
}

void despedida(clientes CLIENTE){
    bzero(buffer,BUFFER_TAM);
    CLIENTE.usuario[strlen(CLIENTE.usuario)-1] = '\0';
    snprintf(buffer,BUFFER_TAM,"\033[31m\t[%s] ha salido de la sala\033[0m\n",CLIENTE.usuario);
    reenviarInformacion(buffer,SV);
}

int parsear_comando(char *comando_tcp, int size_comando_sdr)
{
    char * token;
    //Asumo que el cliente envía su comando en un string con el siguiente formato:
    //                  /fmradio/<frecuencia en MHz>/
    token = strtok(comando_tcp, "/");

    if(strcmp(token, "fmradio")){
        //Si el comando NO es tipo fmradio retorno -1
        printf("No es un comando\n");
        return -1;
    }
    
    char comando_sdr[BUFFER_TAM];
    bzero(comando_sdr,BUFFER_TAM);
    printf("1\n");
    token = strtok(NULL, "/");
    printf("2\n");
    snprintf(comando_sdr, size_comando_sdr, modeloFM, token);
    printf("3\n");
    printf("4\n");
    comando_sdr[strlen(comando_sdr)-1] = ' ';
    printf("El comando es [%s]",comando_sdr);
    
    pclose(pipe_fp);
    pipe_fp = popen(comando_sdr, "r");
    return 1;
}
        


void * clienteHilo(void * cliente){
    
    clientes * auxiliar = (clientes *)cliente;
    
    int sockets = auxiliar->socket;
    
    //recibo mensajes por TCP
    while(1){
        int bytes_recibidos = recv(
            sockets,
            buffer,
            BUFFER_TAM,
            0
                            );                            
        if(bytes_recibidos<=0){
            //printf("Salgo del hilo\n");
            despedida(*auxiliar);
            limpiarHilo(sockets);
            close(sockets);
            pthread_exit(NULL);
        }
        
        printf("RECIBIDO: [%s]\n",buffer);
        
        parsear_comando(buffer,BUFFER_TAM);
        
        buffer[bytes_recibidos] = '\0';
        
        reenviarInformacion(buffer,*auxiliar);
        /*
        printf("Recibidos %d bytes (%s) por el cliente del socket %d usuario %s\n",
                bytes_recibidos,
                buffer,
                socket,
                auxiliar->usuario
        );
        */
    }
}

int main(void){
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    int reuseaddr = 1;
    int error;
    // setsockopt permite reutilizar varias veces el mismo BIND
    error = setsockopt(
        socket_TCP,
        SOL_SOCKET,
        SO_REUSEADDR,
        &reuseaddr,
        sizeof(int)
            );
    
    if ( error == -1) {
        perror("setsockopt");
        exit(1);
    }
    
    direccion_servidor_TCP.sin_family = AF_INET;
    direccion_servidor_TCP.sin_port = htons(PUERTO_TCP);
    direccion_servidor_TCP.sin_addr.s_addr = INADDR_ANY;
    bzero(&(direccion_servidor_TCP.sin_zero),8);
    
    printf("Preparando bind\n");
    
    error = bind(
        socket_TCP,
         (struct sockaddr *)    &direccion_servidor_TCP,
         sin_tam
    );
    
    if (error == -1){
        printf("bind\n\n");
        exit(1);
    }
    
    printf("Preparando listen\n");
    
    listen(
        socket_TCP,
        10
    );    
    pipe_fp = popen("rtl_fm -M wbfm -f 89.1M", "r");
    output_file = fopen("output.iq", "wb");

    shmid = shmget( IPC_PRIVATE, CLIENTES_MAX, IPC_CREAT | 0666 );
    datos = (struct clientes *)shmat(shmid, NULL, 0);
    
    shmbufid = shmget( IPC_PRIVATE, BUFFER_TAM, IPC_CREAT | 0666 );
    buffer = (char *)shmat(shmbufid, NULL, 0);
    
    chatHiloCrear();
    streamHiloCrear();
    
    printf("Preparando accept\n");
    
    int socket_TCP_cliente;
    
    while(1){
        
        mostrarConexiones();
        
        socket_TCP_cliente = accept(
            socket_TCP,
            (struct sockaddr *) &direccion_cliente_TCP,
            &sin_tam
                             );
        
        //printf("Recibida conexion por TCP\n");
        
        int posicion = tomarHilo(socket_TCP_cliente, direccion_cliente_TCP.sin_addr.s_addr);
        
        (datos+posicion)->posicion = posicion;
        
        send(
            (datos+posicion)->socket,
            "Conexion Aceptada\nIngrese su nombre\n",
            38,
            0
        );
        bzero(buffer,BUFFER_TAM);
        int bytes_recibidos = recv(
                (datos+posicion)->socket,
                buffer,
                BUFFER_TAM,
                0
                            );
        buffer[bytes_recibidos-1] = ':';
        buffer[bytes_recibidos] = ' ';
        strcpy((datos+posicion)->usuario,buffer);
        
        pthread_t thread_id;
        
        //printf("Voy a tomar el hijo en la posicion %d (threadid %ld)\n",posicion,thread_id);

        pthread_create(
            &thread_id,
            NULL,
            clienteHilo,
            &(*(datos+posicion))
        );
    }
    

    return 0;
}
