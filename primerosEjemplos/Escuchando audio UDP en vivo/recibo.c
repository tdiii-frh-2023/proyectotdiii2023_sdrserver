#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <vlc/vlc.h>

#define BUF_SIZE 1024
#define PORT 10101

void receive_audio(const char *output_file);

int main() {
    // Ruta del archivo de salida
    const char *output_file = "recibir.mp3";

    // Recibir el archivo MP3 en tiempo real
    if (!fork()){
        receive_audio(output_file);
    }
    usleep(100000);
    
    libvlc_instance_t *vlc;
    libvlc_media_player_t *media_player;
    libvlc_media_t *media;

    // Inicializar la instancia de libvlc
    vlc = libvlc_new(0, NULL);

    // Crear un objeto de media a partir del archivo MP3
    media = libvlc_media_new_path(vlc, "recibir.mp3");

    // Crear un reproductor de medios
    media_player = libvlc_media_player_new_from_media(media);

    // Liberar el objeto de medios, ya que ya no lo necesitamos
    libvlc_media_release(media);

    // Establecer el reproductor de medios al estado de reproducción
    libvlc_media_player_play(media_player);

    // Esperar un tiempo suficiente para que VLC inicie la reproducción (puedes ajustar esto según tus necesidades)
    sleep(5);

    // Detener la reproducción y liberar recursos
    libvlc_media_player_stop(media_player);
    libvlc_media_player_release(media_player);
    libvlc_release(vlc);

    return 0;
}

void receive_audio(const char *output_file) {
    // Crear el socket UDP
    int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_fd == -1) {
        perror("Error al crear el socket");
        exit(EXIT_FAILURE);
    }

    // Configurar la dirección del servidor
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(PORT);
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);

    // Enlazar el socket a la dirección del servidor
    if (bind(socket_fd, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
        perror("Error al enlazar el socket");
        close(socket_fd);
        exit(EXIT_FAILURE);
    }

    // Abrir el archivo de salida
    FILE *output_file_ptr = fopen(output_file, "wb");
    if (!output_file_ptr) {
        perror("Error al abrir el archivo de salida");
        close(socket_fd);
        exit(EXIT_FAILURE);
    }

    char buffer[BUF_SIZE];
    ssize_t bytes_received;
    

    while (1) {
        // Recibir datos por UDP
        bytes_received = recvfrom(
            socket_fd,
            buffer,
            BUF_SIZE,
            0,
            NULL,
            NULL
        );

        if (bytes_received == -1) {
            perror("Error al recibir datos por UDP");
            break;
        }

        // Escribir los datos en el archivo de salida
        fwrite(buffer, 1, bytes_received, output_file_ptr);

        // Simular tiempo real (puedes ajustar esto según tu lógica)
        usleep(10000);  // 10 ms
    }

    // Cerrar el archivo de salida
    fclose(output_file_ptr);
    // Cerrar el socket
    close(socket_fd);
    
}
