#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <termios.h>
#include <string.h>

#define PUERTO_TCP (int)10103
#define PUERTO_UDP (int)21012
#define BUFFER_TAM (int)1024
#define BUFFER_TAM_UDP (int)30000
#define CLIENTES_MAX (int)5

char buffer[BUFFER_TAM];
char bufferUDP[BUFFER_TAM_UDP];

int puerto;

int main(){
    
    int socket_TCP;
    
    int socket_UDP;
    
    int sin_tam = sizeof(struct sockaddr);
    
    struct sockaddr_in direccion_servidor;
    struct hostent * entidad_anfitrion;
    
    entidad_anfitrion = gethostbyname("localhost");
    
    if (entidad_anfitrion == NULL) {
        fprintf(stderr, "Error al obtener la información del host\n");
        exit(1);
    }
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    
    if ( socket_TCP == -1 ){
        printf("socket\n\n");
        exit(1);
    }
    
    direccion_servidor.sin_family = AF_INET;
    direccion_servidor.sin_port = htons(PUERTO_TCP);
    direccion_servidor.sin_addr = *(struct in_addr *)entidad_anfitrion->h_addr;
    bzero(&(direccion_servidor.sin_zero),8);
    
    int error;
    
    printf("Preparando connect\n");
    
    error = connect(
        socket_TCP,
        (struct sockaddr *) &direccion_servidor,
        sin_tam
                );
    
    if ( error == -1 ){
        printf("connect\n\n");
        exit(1);
    }
    
    FILE *vlcPipe = popen("cvlc --quiet -", "w");
    
    int bytes_recibidos = recv(
        socket_TCP,
        buffer,
        BUFFER_TAM,
        0
                        );
    
    if(bytes_recibidos){
        buffer[bytes_recibidos] = '\0';
        
        printf("Recibidos %d bytes \tContenido : %s\n",
                bytes_recibidos,
                buffer
        );
    }
    
    fgets(buffer,BUFFER_TAM,stdin);
    send(
        socket_TCP,
        buffer,
        strlen(buffer),
        0
    );
    
    if(!fork()){
        //hijo
        //envio mensajes por TCP
        while(1){
            bzero(buffer,BUFFER_TAM);
            fgets(buffer,BUFFER_TAM,stdin);
            send(
                socket_TCP,
                buffer,
                strlen(buffer),
                0
            );
        }
    }
    if(!fork()){
        //hijo
        //recibo mensajes por TCP
        while(1){
            bytes_recibidos = recv(
                socket_TCP,
                buffer,
                BUFFER_TAM,
                0
                                );
            
            if(bytes_recibidos){
                buffer[bytes_recibidos] = '\0';
                
                /*printf("Recibidos %d bytes \tContenido : %s\n",
                        bytes_recibidos,
                        buffer
                );
                */
                printf("%s",buffer);
            }
        }
    }
    if(!fork()){
        //hijo
        //recibo mensajes por UDP
        socket_UDP = socket(
            AF_INET,
            SOCK_DGRAM,
            0
                    );
        struct sockaddr_in direccion_servidor_UDP;
        direccion_servidor_UDP.sin_family = AF_INET;
        direccion_servidor_UDP.sin_port = htons(PUERTO_UDP);
        direccion_servidor_UDP.sin_addr.s_addr = htonl(INADDR_ANY);
        bzero(&(direccion_servidor_UDP.sin_zero),8);
        int error = bind(socket_UDP, (struct sockaddr *)&direccion_servidor_UDP, sizeof(direccion_servidor_UDP));;
        int contador = 0;
        while(error == -1){
            printf("Puerto ocupado, busco otro\n");
            contador++;
            direccion_servidor_UDP.sin_port = htons(PUERTO_UDP+contador);
            error = bind(socket_UDP, (struct sockaddr *)&direccion_servidor_UDP, sizeof(direccion_servidor_UDP));
            printf("error = %d\n",error);
        }
        puerto = PUERTO_UDP+contador;
        printf("Bindeado en %d\n",puerto);
        
        /*if (error == -1){
            printf("bind\n\n");
            exit(1);
        }*/
        while(1){
            bytes_recibidos = recvfrom(
                socket_UDP,
                bufferUDP,
                BUFFER_TAM_UDP,
                0,
                (struct sockaddr *)&direccion_servidor_UDP,
                &sin_tam);
            //printf("Recibido %d bytes\n",bytes_recibidos);
            //printf("Escrito en buf %d bytes\n",bytes_recibidos);
            int escrito = fwrite(bufferUDP, sizeof(char), bytes_recibidos, vlcPipe);
            //printf("Escribí %d bytes\n",escrito);
        }
    }
    //padre, proceso libre
    while(1);

    return 0;
}
