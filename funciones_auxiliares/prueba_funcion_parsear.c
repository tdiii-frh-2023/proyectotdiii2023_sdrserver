#include <stdio.h>
#include <stdlib.h>
#include "funciones_auxiliares.h"


void main(void)
{
    char comando_recibido[20] = "/fmradio/102.3/";  
    char comando_para_sdr[200];
    int rv;

    rv = parsear_comando(comando_recibido, comando_para_sdr, 200);

    printf("%s\nrv=%d\n", comando_para_sdr, rv);
}