#ifndef CLIENTE_UTILIDADES_H
#define CLIENTE_UTILIDADES_H

#define MSG_SIZE 1000

/****************************************************
Descripción: Pide nombre a usuario y lo envia por TCP
Argumento: Socket TCP
Retorna: Nada
*****************************************************/
void choose_name(int, char*);

/****************************************************
Descripción: Handler Señal SIGINT. Termina proceso
Argumento: No Utilizado
Retorna: Nada
*****************************************************/
void handler_int (int);

struct msgbuf{
    long mtype;
    char mdata[MSG_SIZE];
}typedef msgbuf_t;

enum{
    NONE,
    TX_CHAT,
    RX_CHAT,
    UDP_STREAM
}typedef msg_type_t;

#endif
