import sys
import os
import ctypes
from ctypes import c_int
import time
import sysv_ipc
from enum import Enum
import signal
import random

from PyQt5 import QtCore, QtGui, QtWidgets

from PyQt5.QtCore import Qt, QThread, pyqtSignal
import sysv_ipc


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 574)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(20, 400, 391, 41))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.banLabel = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.banLabel.setObjectName("banLabel")
        self.horizontalLayout.addWidget(self.banLabel)

        self.UsernameComboBox = QtWidgets.QComboBox(self.horizontalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.UsernameComboBox.sizePolicy().hasHeightForWidth())
        self.UsernameComboBox.setSizePolicy(sizePolicy)
        self.UsernameComboBox.setObjectName("UsernameComboBox")
        self.horizontalLayout.addWidget(self.UsernameComboBox)

        spacerItem = QtWidgets.QSpacerItem(28, 36, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)

        self.BanBtn = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.BanBtn.setObjectName("BanBtn")
        self.horizontalLayout.addWidget(self.BanBtn)

        self.horizontalLayoutWidget_3 = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(20, 460, 391, 77))
        self.horizontalLayoutWidget_3.setObjectName("horizontalLayoutWidget_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")

        self.radioLabel1 = QtWidgets.QLabel(self.horizontalLayoutWidget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.radioLabel1.sizePolicy().hasHeightForWidth())
        self.radioLabel1.setSizePolicy(sizePolicy)
        self.radioLabel1.setObjectName("radioLabel1")
        self.horizontalLayout_3.addWidget(self.radioLabel1)
        
        self.RadioSpinBox = QtWidgets.QDoubleSpinBox(self.horizontalLayoutWidget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.RadioSpinBox.sizePolicy().hasHeightForWidth())
        self.RadioSpinBox.setSizePolicy(sizePolicy)
        self.RadioSpinBox.setObjectName("RadioSpinBox")
        self.horizontalLayout_3.addWidget(self.RadioSpinBox)
        self.RadioSpinBox.setSuffix(' MHz')
        self.RadioSpinBox.setMinimum(80)
        self.RadioSpinBox.setDecimals(1)
        self.RadioSpinBox.setSingleStep(.3)
        self.RadioSpinBox.setMaximum(120)
        self.RadioSpinBox.lineEdit().setReadOnly(True)

        self.radioLabel2 = QtWidgets.QLabel(self.horizontalLayoutWidget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.radioLabel2.sizePolicy().hasHeightForWidth())
        self.radioLabel2.setSizePolicy(sizePolicy)
        self.radioLabel2.setObjectName("radioLabel2")
        self.horizontalLayout_3.addWidget(self.radioLabel2)
        
        spacerItem1 = QtWidgets.QSpacerItem(28, 36, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        
        self.tuneFrequencyBtn = QtWidgets.QPushButton(self.horizontalLayoutWidget_3)
        self.tuneFrequencyBtn.setObjectName("tuneFrequencyBtn")
        self.horizontalLayout_3.addWidget(self.tuneFrequencyBtn)
        self.tuneFrequencyBtn.clicked.connect(self.tuneFrequency)
        
        self.gridLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(20, 10, 761, 281))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")

        self.chatLabel = QtWidgets.QLabel(self.gridLayoutWidget)
        self.chatLabel.setTextFormat(QtCore.Qt.AutoText)
        self.chatLabel.setObjectName("chatLabel")
        self.gridLayout.addWidget(self.chatLabel, 0, 0, 1, 1)

        self.chatTextEdit = QtWidgets.QTextEdit(self.gridLayoutWidget)
        self.chatTextEdit.setFocusPolicy(QtCore.Qt.NoFocus)
        self.chatTextEdit.setReadOnly(False)
        self.chatTextEdit.setObjectName("chatTextEdit")
        self.gridLayout.addWidget(self.chatTextEdit, 1, 0, 1, 1)

        self.horizontalLayoutWidget_4 = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget_4.setGeometry(QtCore.QRect(20, 310, 761, 77))
        self.horizontalLayoutWidget_4.setObjectName("horizontalLayoutWidget_4")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.messageTextEdit = QtWidgets.QTextEdit(self.horizontalLayoutWidget_4)
        self.messageTextEdit.setObjectName("messageTextEdit")
        self.horizontalLayout_4.addWidget(self.messageTextEdit)


        self.sendBtn = QtWidgets.QPushButton(self.horizontalLayoutWidget_4)
        self.sendBtn.setObjectName("sendBtn")
        self.horizontalLayout_4.addWidget(self.sendBtn)
        self.sendBtn.clicked.connect(self.sendMessage)

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        #We create the message queue used to communicate with the client subprocess in c
        self.createIPC()
        #Worker thread emits a data_received signal, which is connected to the receiveMessage method
        self.worker_thread = WorkerThread(self.msg_q)
        self.worker_thread.data_received.connect(self.receiveMessage)
        self.worker_thread.start()
        #The last step is to invoke the client subprocess
        self.callClientSubprocess()
        MainWindow.closeEvent = self.endSubprocesses

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.banLabel.setText(_translate("MainWindow", "Nombre de usuario:"))
        self.BanBtn.setText(_translate("MainWindow", "Expulsar"))
        self.radioLabel1.setText(_translate("MainWindow", "Radio:"))
        self.radioLabel2.setText(_translate("MainWindow", "MHz"))
        self.tuneFrequencyBtn.setText(_translate("MainWindow", "Sintonizar"))
        self.chatLabel.setText(_translate("MainWindow", "Chat"))
        self.messageTextEdit.setPlaceholderText(_translate("MainWindow", "Escriba su mensaje aquí."))
        self.sendBtn.setText(_translate("MainWindow", "Enviar"))

    def createIPC(self):
        self.msg_q = sysv_ipc.MessageQueue(sysv_ipc.IPC_PRIVATE, sysv_ipc.IPC_CREAT | sysv_ipc.IPC_EXCL)
        print("message queue:", self.msg_q)
        self.msg_q_c = c_int(self.msg_q.id)
        message_queue= self.msg_q

    def callClientSubprocess(self):
        clibrary = ctypes.CDLL("/home/gabriel/Desktop/clibrary.so")
        cli_args = (ctypes.c_char_p * len(sys.argv))()
        cli_args[:] = [s.encode('utf-8') for s in sys.argv]

        self.pid_client_subprocess = clibrary.start(c_int(len(sys.argv)), cli_args, self.msg_q_c)
        print(self.pid_client_subprocess)
        
    #pruebas pendientes
    def sendMessage(self):
        message = self.messageTextEdit.toPlainText() + '\n\0'
        self.messageTextEdit.clear()
        b_message = message.encode('utf-8')
        self.msg_q.send(b_message, MsgType.TX_CHAT)

    def tuneFrequency(self):
        frequency = str(self.RadioSpinBox.value())
        command = '/fmradio/'+frequency+'M'+'\n\0'
        print("Enviando comando para cambiar frecuencia")
        print(command)
        b_message = command.encode('utf-8')
        self.msg_q.send(b_message, MsgType.TX_CHAT)

    def receiveMessage(self, data):
        # index_null_character = str(data[0]).find('\0')
        # decoded_b_message = data[0].decode('utf-8')
        # message = decoded_b_message[:index_null_character]
        if data[1] == MsgType.RX_CHAT._value_:
            b_message = data[0][:data[0].index(b'\0')]
            message = b_message.decode('utf-8')
            self.chatTextEdit.append(message)
            print(message)
            # index_null_character = str(data[0]).find('\0')
            # decoded_b_message = data[0].decode('utf-8')
            # message = decoded_b_message[:index_null_character]
            # self.chatTextEdit.append(message)
            # print(message)

    def endSubprocesses(self, event):
        import time
        os.kill(self.pid_client_subprocess, signal.SIGINT)
        time.sleep(1)
        os.kill(self.pid_client_subprocess, signal.SIGKILL)
        print("muere ", self.pid_client_subprocess, "\n")
        

class MsgType(Enum):
    TX_CHAT = 1
    RX_CHAT = 2
    UDP_STREAM = 3

#Worker thread will wait for messages incoming from the chat processes and notify the Main Window
class WorkerThread(QThread):
    data_received = pyqtSignal(list)

    def __init__(self, msg_q):
        super(WorkerThread, self).__init__() #Allows me to reuse code from parent class
        self.msg_q = msg_q

    def run(self):
        while True:
            try:
                message, message_type = self.msg_q.receive()
                data = [message, message_type]
                self.data_received.emit(data)
            except sysv_ipc.ExistentialError:
                break #if the message queue ceases to exist the thread must close
    

global pid_subprocess

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    #pid_subprocess = ui.pid_client_subprocess
    #app.aboutToQuit.connect(endSubprocesses) #When the window is closed, endSubprocesses is called
    MainWindow.show()
    sys.exit(app.exec_())