/*
Uso: llamar a server y mandar como argumento la IP de broadcast
Proceso LECTURA: Lee archivo y escribe en la Shared Memory. Luego se reemplazará el archivo por la lectura del SDR.
Proceso UDP: Envía la información de la Shared Memory por Broadcast UDP. 
Proceso TCP: Se genera conexión TCP. Acepta cliente y le envía el puerto UDP. Espera comandos.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

//Buf size: LENGTH*RATE*SIZE*CHANNELS/8
#define LENGTH 1    //How many seconds of speech to store
#define RATE 20000  //Sampling Rate
#define SIZE 8      //Sample Size
#define CHANNELS 2  //Channels
#define TCP_PORT 2225
#define UDP_PORT 10005
#define BACKLOG 10

void shared_memory (void);          //Creates Shared Memory
int set_tcp (void);                 //Creates TCP Socket
void tcp_process (int);             //Accepts TCP
int set_udp (void);                 //Creates TCP Socket
void udp_process (int, struct sockaddr_in); //Accepts TCP
void read_file (int);
void handler_usr2 (int);
void handler_child (int);

char buf[LENGTH*RATE*SIZE*CHANNELS/8];  //This buffer holds the digitized audio
int childs_done = 0;   //Number of Childs that finished reading
int new_data = 0;   //1 if children have to read new data
char * data;

int main()
{
    int socketfd_tcp, socketfd_udp, newfd;
    struct sockaddr_in their_addr_udp, their_addr_tcp;
    int child_id, udp_port, command, numbytes = 1;
    int sin_size = sizeof(struct sockaddr);

    //Create Shared Memory
    shared_memory();
    printf("\x1b[32mMemory Shared Creada\x1b[0m\n");

    if (!(child_id = fork()))
    {
        //Capturo SIGUSR2
        signal (SIGUSR2, handler_usr2);

        //Set TCP
        socketfd_tcp = set_tcp();
        
        //Set UDP
        socketfd_udp = set_udp();

        //Accept
        if ((newfd = accept(socketfd_tcp, (struct sockaddr *) &their_addr_tcp, &sin_size)) == -1)
        {
            perror("accept");
            exit(1);
        }

        if(!fork())
        {
            printf("\x1b[32mConexion Establecida\x1b[0m\n");

            if ((recv(newfd, &udp_port, sizeof(int), 0)) == -1)
            {
                perror("Receive Port UDP");
                exit(1);
            }
            printf("\x1b[32mRecibido Puerto UDP: %d\x1b[0m\n",udp_port);

            if(!fork())
            {
                //Receive Commands TCP
                tcp_process(newfd);

                printf("\x1b[31mConexion perdida desde %s\x1b[0m\n", inet_ntoa(their_addr_tcp.sin_addr));
                kill(SIGKILL, getppid());
                close (newfd);
                exit(0);
            }

            //Receive Port
            their_addr_udp = their_addr_tcp;
            their_addr_udp.sin_port = htons(udp_port);

            //Send UDP
            while(1)
            {
                udp_process(socketfd_udp, their_addr_udp);
            }
        }
    }
    //Read File
    read_file(child_id);

    return 0;
}


void shared_memory (void)
{
    key_t key_sh;
    int shmid;

    //Create Shared Memory
    if ((key_sh = ftok("proyecto_server.c", 'R')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((shmid = shmget (key_sh, LENGTH*RATE*SIZE*CHANNELS/8, 0644 | IPC_CREAT)) == -1)
    {
        perror("shmget");
        exit(1);
    }

    if ((data = shmat(shmid, 0, 0)) == (char *) (-1))
    {
        perror("shmat");
        exit(1);
    }
    return;
}

int set_tcp (void)
{
    int socketfd_tcp;
    struct sockaddr_in my_addr_tcp;
    
    //Create Socket
    printf("\x1b[32mTCP Creandose...\x1b[0m\n");
    if ((socketfd_tcp = socket(AF_INET, SOCK_STREAM ,0)) == -1) 
    {
        perror("socket TCP");
        exit(1);
    }
    printf("\x1b[32mTCP Creado...\x1b[0m\n");
    
    //set my__addr_tcp
    my_addr_tcp.sin_family = AF_INET;
    my_addr_tcp.sin_port = htons(TCP_PORT);
    my_addr_tcp.sin_addr.s_addr = INADDR_ANY;
    bzero(& (my_addr_tcp.sin_zero), 8);
    
    //Bind Port
    if (bind(socketfd_tcp, (struct sockaddr *) & my_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("bind TCP");
        exit(1);
    }
    
    //Indicate Queue size
    if (listen(socketfd_tcp, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }
    
    return socketfd_tcp;
}

void tcp_process (int newfd)
{
    int command, numbytes = 1;
        
    //Wait for commands
    while(numbytes != 0)
    {
        if (numbytes = (recv(newfd, &command, sizeof(int), 0)) == -1)
        {
            perror("recv TCP");
            exit(1);
        }

        //ACA SE AGREGA CODIGO PARA PROCESAR COMANDOS
    }

    return;
}  

int set_udp ()
{
    int socketfd_udp;

    //Create Socket
    if ((socketfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket UDP");
        exit(1);
    }

    return socketfd_udp;
}

void udp_process (int socketfd_udp, struct sockaddr_in their_addr_udp)
{   
    int i;
    if (new_data == 1)
    {
        strcpy(buf, data);

        if ((i = sendto(socketfd_udp, buf, LENGTH*RATE*SIZE*CHANNELS/8, 0, (struct sockaddr *) &their_addr_udp, sizeof(struct sockaddr))) == -1)
        {
            perror("sendto UDP");
            exit(1);
        }
        printf("\e[34mEnviando por UDP %d bytes\e[0m\n", i);
        new_data = 0;
    }
    
    return;
}

void read_file (int child_id)
{
    const char * input_file = "audio.wav";
    FILE * fd;
    size_t bytes_read;
    
    if ((fd = fopen(input_file, "rb")) == NULL)
    {
        perror("Open Input File");
        exit(1);
    }
    
    while ((bytes_read = fread(buf, 1, LENGTH*RATE*SIZE*CHANNELS/8, fd)) > 0)
    {
        strcpy(data, buf);
        kill (child_id, SIGUSR2);
        sleep(1); //Simulate real time
    }
    
    fclose(fd);

    return;
}

void handler_usr2 (int val)
{
    new_data = 1;
    return;
}

void handler_child (int val)
{
    wait(0);
    return;
}