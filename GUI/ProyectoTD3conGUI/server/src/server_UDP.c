#include <stdlib.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "server_UDP.h"

#define DATA_SIZE 64000
#define N 3     //File Size: DATA_SIZE * N

char buf[DATA_SIZE*N]; //This buffer holds the digitized audio

int set_udp ()
{
    int socketfd_udp;

    //Create Socket
    if ((socketfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket UDP");
        exit(1);
    }

    return socketfd_udp;
}

void udp_process (int socket_udp, struct sockaddr_in their_addr_udp)
{   
    int i=0, bytes_leidos;
    char * input_file = "output.wav";
    FILE * fd;

    if (new_data == 1)
    {
        new_data = 0;

        //Open file
        if ((fd = fopen(input_file, "rb")) == NULL)
        {
            perror("Open File");
            exit(1);
        }

        //Send audio via UDP
        while ((bytes_leidos = fread(buf, 1, DATA_SIZE, fd)) > 0)
        {
            if ((sendto(socket_udp, buf, bytes_leidos, 0, (struct sockaddr *)&their_addr_udp, sizeof(struct sockaddr))) == -1)
            {
                perror("sendto");
                return;
            }
            usleep(1000);

            //printf("\033[33mEnviando %d bytes al puerto %d\033[0m\n", bytes_leidos, ntohs(their_addr_udp.sin_port));
        }

        //Close file
        fclose(fd);

    }
    
    return;
}
