#ifndef CLIENTE_UTILIDADES_H
#define CLIENTE_UTILIDADES_H

/****************************************************
Descripción: Pide nombre a usuario y lo envia por TCP
Argumento: Socket TCP
Retorna: Nada
*****************************************************/
void choose_name(int);

/****************************************************
Descripción: Handler Señal SIGINT. Termina proceso
Argumento: No Utilizado
Retorna: Nada
*****************************************************/
void handler_int (int);

#endif
