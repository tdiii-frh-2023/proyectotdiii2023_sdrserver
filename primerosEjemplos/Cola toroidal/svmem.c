#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#define PUERTO_TCP (int)10103
#define BUFFER_TAM (int)32
#define COLATAM (int)8
#define COLAELEMTAM (int)32


char buffer[BUFFER_TAM];

int contenido = 0;
int posicionContenido = 0;
int posicionEnvio = 0;
char cola[COLATAM][COLAELEMTAM] = {0};
int leido = 1;

FILE * archivoWAV;

void nuevoElemento(){
    posicionContenido = contenido % COLATAM;
    
    if (!leido)  return;
    
    printf("Ahora en la posicion %d\n",posicionContenido);
    printf("Posicion total %ld bytes\n",ftell(archivoWAV));
    bzero(buffer,BUFFER_TAM);
    leido = fread(
        buffer,
        1,
        BUFFER_TAM,
        archivoWAV
    );
    printf("Leidos %d bytes\n",leido);
         
    
    printf("Guardando en cola[%d]\n",posicionContenido);
    strcpy(
        cola[posicionContenido],
        buffer
    );
    printf("Guardado en cola\n");
    
    contenido++;
}

char * obtenerMensaje(){
    char * nuevoElemento = cola[posicionContenido];
    /*
    printf("Elemento %s cargado de la posicion %d\n",nuevoElemento,posicionContenido);
    */
    return nuevoElemento;
}

void primeraCarga(){
    
    archivoWAV = fopen("datosLegibles.txt","r+b");
    
    for (int i = 0 ; i<COLATAM/2 ; i ++ ){
        nuevoElemento();
    }
}


int main(void){
    
    int socket_TCP;
    int socket_UDP;
    
    int sin_tam = sizeof(struct sockaddr);

    struct sockaddr_in direccion_servidor_TCP, direccion_cliente_TCP;
    struct sockaddr_in direccion_servidor_UDP, direccion_cliente_UDP;
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    int reuseaddr = 1;
    int error;
    // setsockopt permite reutilizar varias veces el mismo BIND
    error = setsockopt(
        socket_TCP,
        SOL_SOCKET,
        SO_REUSEADDR,
        &reuseaddr,
        sizeof(int)
            );
    
    if ( error == -1) {
        perror("setsockopt");
        exit(1);
    }
    
    direccion_servidor_TCP.sin_family = AF_INET;
    direccion_servidor_TCP.sin_port = htons(PUERTO_TCP);
    direccion_servidor_TCP.sin_addr.s_addr = INADDR_ANY;
    bzero(&(direccion_servidor_TCP.sin_zero),8);
    
    printf("Preparando bind\n");
    
    error = bind(
        socket_TCP,
         (struct sockaddr *)    &direccion_servidor_TCP,
         sin_tam
    );
    
    if (error == -1){
        printf("bind\n\n");
        exit(1);
    }
    
    printf("Preparando listen\n");
    
    listen(
        socket_TCP,
        10
    );

    printf("Preparando accept\n");
    
    int socket_TCP_cliente;
    
    socket_TCP_cliente = accept(
        socket_TCP,
        (struct sockaddr *) &direccion_cliente_TCP,
        &sin_tam
                            );
    
    int bytes_recibidos;
    int loop = 1;
    
    primeraCarga();
    
    while(posicionContenido-posicionEnvio){
        
        printf("posEnvio %d vs posContenido %d\n",posicionEnvio,posicionContenido);
        
        posicionEnvio++;
        
        nuevoElemento();
        char * mensaje = obtenerMensaje();
        
        send(
            socket_TCP_cliente,
            buffer,
            leido,
            0
        );
        
        getchar();
        /*
        printf("Enviado %s (%d bytes)\n",
               buffer,
               strlen(buffer)
        );
        */

        bytes_recibidos = recv(
            socket_TCP_cliente,
            buffer,
            BUFFER_TAM,
            0
                            );
    }
    
    close(socket_TCP_cliente);
    close(socket_TCP);
    

    return 0;
}
