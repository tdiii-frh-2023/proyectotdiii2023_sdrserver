#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#define PUERTO_TCP (int)10103
#define BUFFER_TAM (int)1024
#define COLATAM (int)8

char buffer[BUFFER_TAM];

int contenido = 0;
int posicionContenido = 0;
int posicionEnvio = 0;
int cola[COLATAM] = {0};

void nuevoElemento(){
    posicionContenido = contenido % COLATAM;
    printf("Ahora en la posicion %d\n",posicionContenido);
    cola[posicionContenido] = contenido;
    contenido++;
}

int obtenerMensaje(){
    int nuevoElemento = cola[posicionContenido];
    printf("Elemento %d cargado de la posicion %d\n",nuevoElemento,posicionContenido);
    
    return nuevoElemento;
}

void primeraCarga(){
    for (int i = 0 ; i<COLATAM/2 ; i ++ ){
        nuevoElemento();
    }
}


int main(void){
    
    int socket_TCP;
    int socket_UDP;
    
    int sin_tam = sizeof(struct sockaddr);

    struct sockaddr_in direccion_servidor_TCP, direccion_cliente_TCP;
    struct sockaddr_in direccion_servidor_UDP, direccion_cliente_UDP;
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    int reuseaddr = 1;
    int error;
    // setsockopt permite reutilizar varias veces el mismo BIND
    error = setsockopt(
        socket_TCP,
        SOL_SOCKET,
        SO_REUSEADDR,
        &reuseaddr,
        sizeof(int)
            );
    
    if ( error == -1) {
        perror("setsockopt");
        exit(1);
    }
    
    direccion_servidor_TCP.sin_family = AF_INET;
    direccion_servidor_TCP.sin_port = htons(PUERTO_TCP);
    direccion_servidor_TCP.sin_addr.s_addr = INADDR_ANY;
    bzero(&(direccion_servidor_TCP.sin_zero),8);
    
    printf("Preparando bind\n");
    
    error = bind(
        socket_TCP,
         (struct sockaddr *)    &direccion_servidor_TCP,
         sin_tam
    );
    
    if (error == -1){
        printf("bind\n\n");
        exit(1);
    }
    
    printf("Preparando listen\n");
    
    listen(
        socket_TCP,
        10
    );

    printf("Preparando accept\n");
    
    int socket_TCP_cliente;
    
    socket_TCP_cliente = accept(
        socket_TCP,
        (struct sockaddr *) &direccion_cliente_TCP,
        &sin_tam
                            );
    
    int bytes_recibidos;
    
    primeraCarga();
    
    while(1){
        
        nuevoElemento();
        int mensaje = obtenerMensaje();
        
        send(
            socket_TCP_cliente,
            &mensaje,
            sizeof(mensaje),
            0
        );
        /*
        printf("Enviado %s (%d bytes)\n",
               buffer,
               strlen(buffer)
        );
        */
        getchar();

        bytes_recibidos = recv(
        socket_TCP_cliente,
        &mensaje,
        sizeof(mensaje),
        0
                        );
    }
    

    return 0;
}
