#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include "server_TCP.h"

#define NAME_SIZE 50
#define TCP_PORT 2242
#define MSG_SIZE 1000
#define BACKLOG 10

char name [NAME_SIZE]; //Client Name
char * data_msg;

int set_tcp (void)
{
    int socketfd_tcp;
    struct sockaddr_in my_addr_tcp;
    int reuseaddr = 1;
    
    //Create Socket
    if ((socketfd_tcp = socket(AF_INET, SOCK_STREAM ,0)) == -1) 
    {
        perror("socket TCP");
        exit(1);
    }

    // setsockopt permite reutilizar varias veces el mismo BIND
    setsockopt(socketfd_tcp, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int));
    
    //set my__addr_tcp
    my_addr_tcp.sin_family = AF_INET;
    my_addr_tcp.sin_port = htons(TCP_PORT);
    my_addr_tcp.sin_addr.s_addr = INADDR_ANY;
    bzero(& (my_addr_tcp.sin_zero), 8);
    
    //Bind Port
    if (bind(socketfd_tcp, (struct sockaddr *) & my_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("bind TCP");
        exit(1);
    }
    
    //Indicate Queue size
    if (listen(socketfd_tcp, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }

    printf("\033[32mTCP Creado en puerto %d...\033[0m\n", TCP_PORT);

    return socketfd_tcp;
}


void tcp_process (int newfd)
{
    int numbytes = 0;
    char msg [MSG_SIZE];
    char final_msg [MSG_SIZE+NAME_SIZE+2];

    //Wait for commands
    do
    {
        if ((numbytes = recv(newfd, msg, MSG_SIZE, 0)) == -1)
        {
            perror("recv TCP");
            exit(1);
        }

        if(numbytes != 0)
            parsear_comando(name, msg);
    }while(numbytes != 0);

    return;
}  


void send_tcp_process(int newfd)
{
    new_data = 0;

    while(1)
    {
        if(new_data)
        {
            new_data = 0;

            //Send msg via TCP
            if (send(newfd, data_msg, MSG_SIZE, 0) == -1)
            {
                perror("send TCP");
                exit(1);
            }
        }
    }
}
