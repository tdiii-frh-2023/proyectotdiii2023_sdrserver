#ifndef SERVER_TCP_H
#define SERVER_TCP_H

extern int new_data;

/****************************************************
Descripción: Crea Socket TCP, Bindea y hace el Listen
Argumento: Ninguno
Retorna: Socket TCP del Server
*****************************************************/
int set_tcp (void);

/****************************************************
Descripción: Bucle. Espera mensajes por TCP
Argumento: Socket TCP asociado al cliente
Retorna: Nada
*****************************************************/
void tcp_process (int);

/*******************************************************
Descripción: Envia mensaje por TCP que obtiene de la SM
Argumento: Socket TCP asociado al cliente
Retorna: Nada
*******************************************************/
void send_tcp_process (int);

/****************************************************
Descripción: Identifica los comandos, coloca mensaje en SM y avisa que hay nuevo chat para enviar
Argumento: Nombre cliente y Mensaje
Retorna: Nada
*****************************************************/
void parsear_comando (char *, char *); //Translates Client Command to SDR Command

#endif
