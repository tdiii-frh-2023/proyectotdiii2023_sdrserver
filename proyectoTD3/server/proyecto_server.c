/*
Uso: llamar a server
Proceso LECTURA: Lee la SDR escribe datos en archivos .wav.
Proceso Accept: Se queda esperando conexiones nuevas por TCP
Proceso UDP: Recibe Puero UDP por TCP. Envía la información de los archivos por UDP. 
Proceso Recv TCP: Espera comandos y chat.
Proceso Send TCP: Envia por TCP mensaje a su cliente. Esto sirve para que todos reciban el chat
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include "server_TCP.h"
#include "server_UDP.h"
#include "utilidades.h"

#define DATA_SIZE 64000
#define N 3     //File Size: DATA_SIZE * N
#define MSG_SIZE 1000
#define TCP_PORT 2242
#define BACKLOG 10

char buf[DATA_SIZE*N]; //This buffer holds the digitized audio
int new_data = 0;   //1 if children have to read new data in output.wav
int child_id = 0;
int send_tcp_pid = 0;
int childs = 0;     //Number of children alive
int udp_children [MAX_CLIENTS]; //Stores UDP Childrens PID
int send_tcp_children [MAX_CLIENTS]; //Stores SEND TCP Childrens PID
char * data;
char * data_msg;
char * msg_data;
FILE * pipe_fp;
int mp_id; //Main Process ID
int accept_pid = 0; //Accept Process ID
int change_freq = 0; //1 if frequency change is needed
char name [NAME_SIZE]; //Client Name

int main()
{
    int socket_tcp, socket_udp, newfd;
    struct sockaddr_in their_addr_udp, their_addr_tcp;
    int udp_port, command, numbytes = 1, bytes_read;
    int sin_size = sizeof(struct sockaddr);
    const char * create_wav = "sox -t raw -r 16k -e signed-integer -b 16 -c 2 -V1 output.iq output.wav";
    const char * init_sdr = "rtl_fm -M wbfm -f 89.1M";
    const char * output_file_name = "output.iq";
    FILE * output_file;

    //Main Process ID
    mp_id = getpid();

    //Capture SIGINT & SIGUSR2
    signal (SIGINT, handler_int);
    signal (SIGUSR2, handler_usr2);

    //Create Shared Memory
    shared_memory_commands();
    shared_memory_msg();

    if (!(child_id = fork()))
    {
        /*-------------------------------------------------------------------------
        ******************************ACCEPT PROCESS*******************************
        -------------------------------------------------------------------------*/

        //Saves PID
        accept_pid = getpid();

        //Clean Children Vectors
        bzero(udp_children, BACKLOG);
        bzero(send_tcp_children, BACKLOG);

        //Capture SIGUSR1 & SIGCHLD
        signal (SIGUSR1, handler_usr1);
        signal (SIGCHLD, handler_child);

        //Set TCP
        socket_tcp = set_tcp();
        
        //Set UDP
        socket_udp = set_udp();

        while(1)
        {
            //Accept
            if ((newfd = accept(socket_tcp, (struct sockaddr *) &their_addr_tcp, &sin_size)) == -1)
            {
                perror("accept");
                exit(1);
            }
            
            if(childs < MAX_CLIENTS)
            {
                childs++;

                if(!(send_tcp_pid = fork()))
                {
                    /*-------------------------------------------------------------------------
                    ******************************SEND TCP PROCESS*****************************
                    -------------------------------------------------------------------------*/
                    send_tcp_process(newfd);
                    exit(0);
                }

                if(!(child_id = fork()))
                {
                    /*-------------------------------------------------------------------------
                    ********************************UDP PROCESS********************************
                    -------------------------------------------------------------------------*/
                    printf("\033[32m\nConexion Establecida con %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));

                    udp_port = get_port(newfd);

                    get_name(newfd, their_addr_tcp); 

                    if(!(child_id = fork()))
                    {
                        /*-------------------------------------------------------------------------
                        *****************************TCP RECV PROCESS******************************
                        -------------------------------------------------------------------------*/
                        //Receive Commands TCP
                        tcp_process(newfd);

                        //Connection Lost
                        close_client(newfd, their_addr_tcp);
                    }       
 
                    //Receive Port
                    their_addr_udp = their_addr_tcp;
                    their_addr_udp.sin_port = htons(udp_port);

                    //Send UDP
                    while(1)
                    {
                        udp_process(socket_udp, their_addr_udp);
                    }
                }

                child_id_vector(child_id, send_tcp_pid);
                child_id = 0;
            }
            else
            {
                close(newfd);
                printf("\033[31mConexion Rechazada: Maximo de Clientes Alcanzado\033[0m\n");
            }

        }
        
    }
/*-------------------------------------------------------------------------
********************************SDR PROCESS********************************
-------------------------------------------------------------------------*/

    printf("\033[36m------CONFIGURANDO SDR------\033[0m\n");

    if ((pipe_fp = popen(init_sdr, "r")) == NULL)
    {
        perror("Error opening pipe");
        return EXIT_FAILURE;
    }

    if ((output_file = fopen(output_file_name, "wb")) == NULL)
    {
        perror("Error opening output file");
        pclose(pipe_fp);
        return EXIT_FAILURE;
    }
    
    while(1)
    {
        //Get Audio
        fflush(pipe_fp);
        bzero(buf,DATA_SIZE*N);
        bytes_read = fread(buf, 1, (DATA_SIZE*N),pipe_fp);
        rewind(output_file);
        fwrite(buf, 1, bytes_read, output_file);

        //Create .wav File
        system(create_wav);

        //New data available
        kill (child_id, SIGUSR1);

        //Change Frequency if required
        if(change_freq)
        {
            change_freq = 0;
            pclose(pipe_fp);
            if ((pipe_fp = popen(data, "r")) == NULL)
            {
                perror("Error opening pipe");
                exit(1);
            }
        }
    }

    return 0;
}
void parsear_comando(char *name, char * msg)
{
    char * token;
    token = strtok(msg, "/");
    char sdr_command[MSG_SIZE];
    char * modeloFM = "rtl_fm -M wbfm -f %s";
    char final_msg [NAME_SIZE+MSG_SIZE+2];
    

    //Command Style: /fmradio/<frecuencia en MHz>/
    if(strcmp(token, "fmradio"))
    {
        //Not a Command
        sprintf(final_msg, "%s: %s", name, msg);
        strcpy(data_msg, final_msg);
        kill(accept_pid, SIGUSR2);
        return;
    }
    
    bzero(sdr_command,MSG_SIZE);
    token = strtok(NULL, "/");
    snprintf(sdr_command, MSG_SIZE, modeloFM, token);
    sdr_command[strlen(sdr_command)-1] = ' ';

    printf("\033[35mEl comando recibido es [%s]\033[0m\n",sdr_command);

    //Msg to send
    sprintf(final_msg, "\033[36m%s cambio la radio a %s\033[0m\n", name, token);
    strcpy(data_msg, final_msg);
    kill(accept_pid, SIGUSR2);
    
    strcpy(data, sdr_command);

    kill(mp_id, SIGUSR2);

    return;
}

