#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#define PUERTO_TCP (int)10103
#define BUFFER_TAM (int)32768
#define COLATAM (int)8

char buffer[BUFFER_TAM];


int main(void){
    
    int socket_TCP;
    int socket_UDP;
    
    int sin_tam = sizeof(struct sockaddr);

    struct sockaddr_in direccion_servidor_TCP, direccion_cliente_TCP;
    struct sockaddr_in direccion_servidor_UDP, direccion_cliente_UDP;
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    int reuseaddr = 1;
    int error;
    // setsockopt permite reutilizar varias veces el mismo BIND
    error = setsockopt(
        socket_TCP,
        SOL_SOCKET,
        SO_REUSEADDR,
        &reuseaddr,
        sizeof(int)
            );
    
    if ( error == -1) {
        perror("setsockopt");
        exit(1);
    }
    
    direccion_servidor_TCP.sin_family = AF_INET;
    direccion_servidor_TCP.sin_port = htons(PUERTO_TCP);
    direccion_servidor_TCP.sin_addr.s_addr = INADDR_ANY;
    bzero(&(direccion_servidor_TCP.sin_zero),8);
    
    printf("Preparando bind\n");
    
    error = bind(
        socket_TCP,
         (struct sockaddr *)    &direccion_servidor_TCP,
         sin_tam
    );
    
    if (error == -1){
        printf("bind\n\n");
        exit(1);
    }
    
    printf("Preparando listen\n");
    
    listen(
        socket_TCP,
        10
    );

    printf("Preparando accept\n");
    
    int socket_TCP_cliente;
    
    socket_TCP_cliente = accept(
        socket_TCP,
        (struct sockaddr *) &direccion_cliente_TCP,
        &sin_tam
                            );
    
    int bytes_recibidos;
    
    FILE * archivoWAV;
    
    archivoWAV = fopen(
        "enviar.mp3",
        "r+b"
                 );
    int leidos=1;
    
    fseek(archivoWAV, 0, SEEK_END);
    long int total = ftell(archivoWAV);
    rewind(archivoWAV);   
    
    
    leidos = fread(
        buffer,
        1,
        BUFFER_TAM,
        archivoWAV
    );
    
    printf("Leidos %d bytes (%ld/%ld)\n",
        leidos,
        ftell(archivoWAV),
        total
    );
        
    send(
        socket_TCP_cliente,
        buffer,
        leidos,
        0
    );

    while(leidos > 0){
        
        printf("ESPERO\n");
        sleep(1);
        printf("SIGO\n");
        
        bytes_recibidos = recv(
            socket_TCP_cliente,
            buffer,
            BUFFER_TAM,
            0
                            );
        leidos = fread(
            buffer,
            1,
            BUFFER_TAM,
            archivoWAV
        );
        
        printf("Leidos %d bytes (%ld/%ld)\n",
            leidos,
            ftell(archivoWAV),
            total
        );
            
        send(
            socket_TCP_cliente,
            buffer,
            leidos,
            0
        );
        /*
        printf("Enviado %s (%d bytes)\n",
               buffer,
               strlen(buffer)
        );
        */
    }
    
    printf("FIN\n");
    fclose(archivoWAV);
    close(socket_TCP_cliente);
    close(socket_TCP);
    

    return 0;
}
