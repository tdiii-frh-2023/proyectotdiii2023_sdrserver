#ifndef SERVER_UTILIDADES_H
#define SERVER_UTILIDADES_H

#define NAME_SIZE 50
#define MAX_CLIENTS 10

extern char * data;
extern char * data_msg;
extern char name [NAME_SIZE]; //Client Name
extern int send_tcp_pid;
extern int udp_children [MAX_CLIENTS]; //Stores UDP Childrens PID
extern int new_data;   //1 if children have to read new data in output.wav
extern int change_freq; //1 if frequency change is needed
extern int accept_pid; //Accept Process ID
extern int send_tcp_children [MAX_CLIENTS]; //Stores SEND TCP Childrens PID
extern int childs;     //Number of children alive

/****************************************************
Descripción: Crea Shared Memory para comandos
Argumento: Ninguno
Retorna: Nada
*****************************************************/
void shared_memory_commands (void);

/****************************************************
Descripción: Crea Shared Memory para mensajes
Argumento: Ninguno
Retorna: Nada
*****************************************************/
void shared_memory_msg (void);

/****************************************************
Descripción: Recibe puerto UDP del cliente por TCP
Argumento: Socket TCP asociado al cliente
Retorna: Puerto UDP
*****************************************************/
int get_port(int);

/****************************************************
Descripción: Recibe puerto nombre del cliente por TCP
Argumento: Socket TCP asociado al cliente y Direccion TCP del cliente
Retorna: Nada
*****************************************************/
void get_name(int, struct sockaddr_in);

/****************************************************
Descripción: Cierra procesos asociados al cliente desconectado
Argumento: Socket TCP asociado al cliente y Direccion TCP del cliente
Retorna: Nada
*****************************************************/
void close_client(int, struct sockaddr_in);

/**********************************************************************
Descripción: Handler Señal SIGUSR1. Envia SIGUSR2 a todos los hijos UDP
Argumento: Ninguno
Retorna: Nada
**********************************************************************/
void handler_usr1 (int);

/****************************************************
Descripción: Handler Señal SIGUSR2
Argumento: No utilizado
Retorna: Nada
*****************************************************/
void handler_usr2 (int);

/****************************************************
Descripción: Handler Señal SIGCHLD. Evita Zombies.
Argumento: No utilizado
Retorna: Nada
*****************************************************/
void handler_child (int);

/****************************************************
Descripción: Handler Señal SIGINT. Termina proceso
Argumento: No utilizado
Retorna: Nada
*****************************************************/
void handler_int (int);

/************************************************************************
Descripción: Guarda PID de procesos en vectores. Actualiza contador Hijos
Argumento: PID del nuevo proceso UDP y del nuevo proceso Send TCP
Retorna: Nada
************************************************************************/
void child_id_vector (int udp_pid, int send_tcp_pid);

#endif
