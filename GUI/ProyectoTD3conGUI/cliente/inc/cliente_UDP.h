#ifndef CLIENTE_UDP_H
#define CLIENTE_UDP_H

/***************************************************************************
Descripción: Crea el Socket UDP, busca puerto disponible y lo manda por TCP
Argumento: Socket TCP
Retorna: Socket UDP
***************************************************************************/
int set_udp_port (int);

/****************************************************
Descripción: Recibe audio por UDP y lo reproduce
Argumento: Socket UDP
Retorna: Nada
*****************************************************/
void receive_udp (int);

#endif
