import numpy as np
import sounddevice as sd

# Specify the file path
file_path = "output.bin"

# Sample rate used during capture (must match the rate used in rtl_fm)
sample_rate = 32000  # 32 kHz (adjust as needed)

# Open the binary file and read its content
with open(file_path, 'rb') as file:
    # Read the binary data into a NumPy array (assuming 16-bit signed integers)
    raw_data = np.fromfile(file, dtype=np.int16)

# Play the audio samples using sounddevice
sd.play(raw_data, sample_rate)

# Wait for audio to finish playing
sd.wait()
