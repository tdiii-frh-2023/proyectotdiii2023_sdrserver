#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
extern int h_errno;

#define DATA_SIZE 64000
#define THEIR_PORT 2268
#define MY_PORT 3333

int main(int argc, char * argv[]) 
{
    char *file_name = "audio.wav";
    int socket_udp, bytes_leidos;
    struct sockaddr_in their_addr, my_addr, their_addr_2;
    struct hostent * he;
    int sinsize = sizeof(struct sockaddr);
    FILE * fp;
    char buf [DATA_SIZE];

    //Abrir el archivo de audio
    if ((fp = fopen(file_name, "rb")) == NULL)
    {
        perror("No se pudo abrir el archivo de audio correctamente\n");
        exit(1);
    }

    //Socket UDP
    if((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        return 1;
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MY_PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr.sin_zero),8);

    if((bind(socket_udp, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))) == -1)
    {
        perror("socket");
        return 1;
    }

    he = gethostbyname(argv[1]);
    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(THEIR_PORT);
    their_addr.sin_addr = * (struct in_addr *)he->h_addr;
    bzero(&(their_addr.sin_zero),8);


    //Se puede mover la ventana para no comenzar desde el principio
    //fseek(fp, 50000000, SEEK_SET);
    int i;

    if ((sendto(socket_udp, buf, bytes_leidos, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1)
    {
        perror("sendto");
        return 1;
    }
    printf("Enviando audio inicial\n");
    
    while (1)
    {
        i = 0;

        for(i=0; i<10; i++)
        {
            bytes_leidos = fread(buf, 1, DATA_SIZE, fp);
            printf("Envio muestra audio %d\n", i);
            if ((sendto(socket_udp, buf, bytes_leidos, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1)
            {
                perror("sendto");
                return 1;
            }
        }

        recvfrom(socket_udp, &bytes_leidos, sizeof(int), 0, (struct sockaddr *)&their_addr_2, &sinsize);
    }

    // Cerrar el archivo
    fclose(fp);

    return 0;
}

