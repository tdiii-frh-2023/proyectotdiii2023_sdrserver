/*********************************************
PARA COMPILAR: gcc udp_recv_mic.c -o nombre -lasound

EJECUTAR CON ./nombre localhost

ESTE PROGRAMA RECIBE AUDIO Y LO REPRODUCE
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <signal.h>

extern int h_errno;

#define DATA_SIZE 128 // TAMAÑO 128 PORQUE 32 MUESTRAS EN 2 CANALES Y 2 BYTES CADA UNA
#define THEIR_PORT 2268
#define MY_PORT 3333
#define PCM_DEVICE "default"

void handlerkill(int val);

snd_pcm_t * handle;
char buf [DATA_SIZE]; //SI LA HAGO LOCAL SE ROMPE XD


int main(int argc, char * argv[]) 
{
    
    int socket_udp, rc, dir;
    unsigned int val;
    struct sockaddr_in their_addr, my_addr;
    struct hostent * he;
    int sinsize = sizeof(struct sockaddr);
    FILE * fp;
    snd_pcm_hw_params_t  *params;
    snd_pcm_uframes_t frames;
    
    signal(SIGINT, handlerkill);

    //Socket UDP
    if((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        return 1;
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MY_PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr.sin_zero),8);

    if((bind(socket_udp, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))) == -1)
    {
        perror("socket");
        return 1;
    }

    he = gethostbyname(argv[1]);
    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(THEIR_PORT);
    their_addr.sin_addr = * (struct in_addr *)he->h_addr;
    bzero(&(their_addr.sin_zero),8);

    /* Abrir el dispositivo PCM para grabación */
    if ((snd_pcm_open(&handle, PCM_DEVICE, SND_PCM_STREAM_CAPTURE, 0)) < 0)
    {
        perror("No se puede abrir el dispositivo PCM: %s\n");
        exit (1);
    }

    /* Configurar los parámetros del dispositivo PCM */
    snd_pcm_hw_params_alloca(&params);
    snd_pcm_hw_params_any(handle, params);
    snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);
    snd_pcm_hw_params_set_channels(handle, params, 2); /* 2 canales (estéreo) */
    val = 44100; /* Frecuencia de muestreo: 44.1kHz */
    snd_pcm_hw_params_set_rate_near(handle, params, &val, &dir);
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(handle, params, &frames, &dir);

    /* Aplicar los parámetros */
    if ((snd_pcm_hw_params(handle, params)) < 0) 
    {
        fprintf(stderr, "Error al configurar los parámetros: %s\n", snd_strerror(rc));
        exit (1);
    }
    
    while (1)
    {
        rc = snd_pcm_readi(handle, buf, frames);
        if (rc == -EPIPE) 
        {
            perror("Error de recuperación de interrupción: \n");
            snd_pcm_prepare(handle);
        } 
        else if (rc < 0) 
        {
            perror("Error al leer desde el dispositivo PCM: \n");
        } 
        else if (rc != (int)frames) 
        {
            perror("Error: no se pudo leer el número completo de fotogramas\n");
        }

        if ((sendto(socket_udp, buf, rc*4, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1)
        {
            perror("sendto");
            return 1;
        }
    }

    return 0;
}

void handlerkill(int val)
{
    printf("Saliendo del programa\n");
    snd_pcm_close(handle);
    exit(0);
}