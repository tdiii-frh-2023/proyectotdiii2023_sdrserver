#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define BUF_SIZE 1024
#define PORT 10101

void send_audio(const char *file_path, int socket_fd, struct sockaddr_in server_address);

int main() {
    // Crear el socket UDP
    int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_fd == -1) {
        perror("Error al crear el socket");
        exit(EXIT_FAILURE);
    }

    // Configurar la dirección del servidor
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(PORT);
    server_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK); // IP de loopback (localhost)

    // Ruta del archivo MP3 a enviar
    const char *file_path = "enviar.mp3";

    // Enviar el archivo MP3 en tiempo real
    send_audio(file_path, socket_fd, server_address);

    // Cerrar el socket
    close(socket_fd);

    return 0;
}

void send_audio(const char *file_path, int socket_fd, struct sockaddr_in server_address) {
    FILE *audio_file = fopen(file_path, "rb");
    if (!audio_file) {
        perror("Error al abrir el archivo de audio");
        exit(EXIT_FAILURE);
    }

    char buffer[BUF_SIZE];
    size_t bytes_read;

    while ((bytes_read = fread(buffer, 1, BUF_SIZE, audio_file)) > 0) {
        ssize_t bytes_sent = sendto(
            socket_fd,
            buffer,
            bytes_read,
            0,
            (struct sockaddr *)&server_address,
            sizeof(server_address)
        );

        if (bytes_sent == -1) {
            perror("Error al enviar datos por UDP");
            break;
        }

        // Simular tiempo real (puedes ajustar esto según tu lógica)
        usleep(10000);  // 10 ms
    }

    // Cerrar el archivo de audio
    fclose(audio_file);
}
