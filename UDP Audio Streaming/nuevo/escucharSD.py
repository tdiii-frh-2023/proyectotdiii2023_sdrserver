import numpy as np
from scipy.io import wavfile

def safe_angle(x, tolerance=1e-8):
    real_part = np.real(x)
    if np.isclose(real_part, 0, atol=tolerance):
        return np.pi / 2 if np.imag(x) > 0 else -np.pi / 2
    else:
        return np.arctan2(np.imag(x), real_part)

def demodulate_iq_to_wav(input_file, output_file, sample_rate):
    # Read IQ data from the file
    iq_data = np.fromfile(input_file, dtype=np.complex64)

    # Demodulate by extracting the phase information
    phase = np.unwrap(safe_angle(iq_data))

    # Compute the frequency deviation (change in phase over time)
    freq_deviation = np.diff(phase)

    # Convert the frequency deviation to audio by integrating
    audio_data = np.cumsum(freq_deviation)

    # Normalize the audio data
    audio_data = audio_data / np.max(np.abs(audio_data))

    # Save the demodulated audio as a WAV file
    wavfile.write(output_file, sample_rate, (audio_data * 32767).astype(np.int16))

if __name__ == "__main__":
    input_iq_file = "datos.bin"  # Replace with the path to your input IQ file
    output_wav_file = "output.wav"  # Replace with the desired path for the output WAV file
    sample_rate = 3200000  # Replace with the actual sample rate used during capture

    demodulate_iq_to_wav(input_iq_file, output_wav_file, sample_rate)
