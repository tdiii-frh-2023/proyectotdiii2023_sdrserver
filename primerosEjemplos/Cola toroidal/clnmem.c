#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <termios.h>
#include <string.h>
#include <vlc/vlc.h>

#define PUERTO_TCP (int)10103
#define BUFFER_TAM (int)32768

char buffer[BUFFER_TAM];

int main(){
    
    int socket_TCP;
    
    int sin_tam = sizeof(struct sockaddr);
    
    struct sockaddr_in direccion_servidor;
    struct hostent * entidad_anfitrion;
    
    entidad_anfitrion = gethostbyname("localhost");
    
    if (entidad_anfitrion == NULL) {
        fprintf(stderr, "Error al obtener la información del host\n");
        exit(1);
    }
    
    socket_TCP = socket(
        AF_INET,
        SOCK_STREAM,
        0
                 );
    
    if ( socket_TCP == -1 ){
        printf("socket\n\n");
        exit(1);
    }
    
    direccion_servidor.sin_family = AF_INET;
    direccion_servidor.sin_port = htons(PUERTO_TCP);
    direccion_servidor.sin_addr = *(struct in_addr *)entidad_anfitrion->h_addr;
    bzero(&(direccion_servidor.sin_zero),8);
    
    int error;
    
    printf("Preparando connect\n");
    
    error = connect(
        socket_TCP,
        (struct sockaddr *) &direccion_servidor,
        sin_tam
                );
    
    if ( error == -1 ){
        printf("connect\n\n");
        exit(1);
    }
    
    int bytes_recibidos=1;
    
    FILE * archivoWAV;
    
    archivoWAV = fopen(
        "recibir.mp3",
        "w+b"
                 );
    if (!fork()){
        
        sleep(5);
        
        libvlc_instance_t *vlcInstance = libvlc_new(0,NULL);

        // Create a media player
        libvlc_media_player_t *mediaPlayer = libvlc_media_player_new(vlcInstance);

        // Load an MP3 file
        libvlc_media_t *media = libvlc_media_new_path(vlcInstance, "recibir.mp3");
        libvlc_media_player_set_media(mediaPlayer, media);
        libvlc_media_release(media);

        // Play the media
        libvlc_media_player_play(mediaPlayer);
        
        while(1);
    }

    while(bytes_recibidos){        
        bytes_recibidos = recv(
        socket_TCP,
        buffer,
        BUFFER_TAM,
        0
                        );
    
        if(bytes_recibidos){
            printf("Recibidos %d bytes\n",
                   bytes_recibidos
            );
            fwrite(
                buffer,
                1,
                bytes_recibidos,
                archivoWAV
            );
        }
        
        send(
            socket_TCP,
            buffer,
            bytes_recibidos,
            0
        );
        /*
        printf("Enviado %d (%d bytes)\n",
               mensaje,
               sizeof(mensaje)
        );
        */
    }
    
    printf("FIN\n");
    fclose(archivoWAV);
    close(socket_TCP);


    return 0;
}
