#include <stdio.h>
#include <stdlib.h>
#include <portaudio.h>

int main() {
    const char *file_path = "buffer.bin"; // Specify the file path
    const int sample_rate = 32000;       // Sample rate in Hz (adjust as needed)
    const int num_channels = 1;          // Mono audio

    FILE *file = fopen(file_path, "rb");
    if (!file) {
        printf("Error: Could not open the file for reading.\n");
        return 1;
    }

    PaError err;
    err = Pa_Initialize();
    if (err != paNoError) {
        printf("PortAudio error: %s\n", Pa_GetErrorText(err));
        return 1;
    }

    PaStream *stream;
    err = Pa_OpenDefaultStream(&stream, num_channels, num_channels, paInt16, sample_rate, paFramesPerBufferUnspecified, NULL, NULL);
    if (err != paNoError) {
        printf("PortAudio error: %s\n", Pa_GetErrorText(err));
        return 1;
    }

    err = Pa_StartStream(stream);
    if (err != paNoError) {
        printf("PortAudio error: %s\n", Pa_GetErrorText(err));
        return 1;
    }

    int16_t buffer[1024]; // Adjust the buffer size as needed
    size_t read_count;

    while ((read_count = fread(buffer, sizeof(int16_t), sizeof(buffer) / sizeof(int16_t), file)) > 0) {
        err = Pa_WriteStream(stream, buffer, read_count);
        if (err != paNoError) {
            printf("PortAudio error: %s\n", Pa_GetErrorText(err));
            break;
        }
    }

    err = Pa_StopStream(stream);
    if (err != paNoError) {
        printf("PortAudio error: %s\n", Pa_GetErrorText(err));
    }

    err = Pa_CloseStream(stream);
    if (err != paNoError) {
        printf("PortAudio error: %s\n", Pa_GetErrorText(err));
    }

    Pa_Terminate();

    fclose(file);
    return 0;
}
