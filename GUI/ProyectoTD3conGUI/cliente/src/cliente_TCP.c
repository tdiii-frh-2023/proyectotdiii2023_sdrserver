#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/msg.h>
#include "cliente_TCP.h"
#include "utilidades.h"

#define MSG_SIZE 1000

int set_tcp (struct sockaddr_in their_addr_tcp)
{
    int socket_tcp;
    
    //Create Socket
    if ((socket_tcp = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket TCP");
        exit(1);
    }
    
    //Connect
    if (connect(socket_tcp, (struct sockaddr *) & their_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("connect TCP");
        exit(1);
    }

    printf ("\e[32mPedido de Conexion hecho\e[0m\n");
    
    return socket_tcp;
}

void tcp_chat_tx (int socket_tcp, int msgqid)
{
    msgbuf_t msg;

    while(1)
    {
        //Wait for user msg
        bzero(&(msg.mdata), MSG_SIZE);
        msgrcv(msgqid, &msg, sizeof(msgbuf_t), TX_CHAT, 0);
        //Send via TCP
        if (send(socket_tcp, msg.mdata, MSG_SIZE, 0) == -1)
        {
            perror("send TCP Port");
            exit(1);
        }
    }

    return;
}

void tcp_chat_rx (int socket_tcp, int msgqid)
{
    msgbuf_t msg;

    while(1)
    {
        //Send via TCP
        if (recv(socket_tcp, msg.mdata, MSG_SIZE, 0) == -1)
        {
            perror("send UDP Port");
            exit(1);
        }

        //Data is sent to the GUI program
        msg.mtype = RX_CHAT;
        msgsnd(msgqid, &msg, sizeof(msgbuf_t), 0);
    }

    return;
}