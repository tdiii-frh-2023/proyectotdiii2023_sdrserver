/*
Uso: llamar a cliente y mandar como argumento la IP (127.0.0.0)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#define LENGTH 1    //How many seconds of speech to store
#define RATE 20000  //Sampling Rate
#define SIZE 8      //Sample Size
#define CHANNELS 2  //Channels
#define TCP_PORT 2225

int set_tcp (struct sockaddr_in);
int set_udp_port (int);
void receive_udp (int);


int main (int argc, char * argv[])
{
    int socket_tcp, socket_udp;
    struct sockaddr_in their_addr_tcp; //Informacion del server
    struct hostent * he; //Direccion a donde se conecta
    int udp_port = 0;
    
    //Reviso cantidad argumentos
    if (argc != 2)
    {
        perror("argumentos");
        exit(1);
    } 
    
    //Convierto nombre Host a IP
    if ((he = gethostbyname(argv[1]))== NULL)
    {
        perror("gethostbyname");
        exit(1);
    }
    
    //Server Information
    their_addr_tcp.sin_family = AF_INET;
    their_addr_tcp.sin_port = htons(TCP_PORT);
    their_addr_tcp.sin_addr = *(struct in_addr *)he->h_addr;
    bzero(&(their_addr_tcp.sin_zero), 8);
    
    //Set TCP
    socket_tcp = set_tcp(their_addr_tcp);
    
    //Set UDP port
    socket_udp = set_udp_port(socket_tcp);
    
    receive_udp(socket_udp);
    
    close (socket_tcp);

    return 0;
}

int set_tcp (struct sockaddr_in their_addr_tcp)
{
    int socket_tcp;
    
    //Create Socket
    if ((socket_tcp = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket TCP");
        exit(1);
    }
    
    //Connect
    if (connect(socket_tcp, (struct sockaddr *) & their_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("connect TCP");
        exit(1);
    }

    printf ("\e[32mPedido de Conexion hecho\e[0m\n");
    
    return socket_tcp;
} 

int set_udp_port(int socket_tcp)
{
    int udp_port = 10000; //Initial UDP Port
    struct sockaddr_in my_addr_udp;
    int socket_udp;
    
    //Create Socket
    if ((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    
    //Set my_addr_udp
    my_addr_udp.sin_family = AF_INET;
    my_addr_udp.sin_port = htons(udp_port);
    my_addr_udp.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr_udp.sin_zero), 8);
    
    //Bind
    while (bind(socket_udp, (struct sockaddr *)&my_addr_udp, sizeof(struct sockaddr))  == -1)
    {
        my_addr_udp.sin_port = htons(udp_port++);
    }
    
    //Inform Server
    if (send(socket_tcp, &udp_port, sizeof(int), 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    printf ("\e[34mPuerto UDP: %d\e[0m\n", udp_port);

    return socket_udp;
}

void receive_udp(int socket_udp)
{
    FILE * fp;
    char buf[LENGTH*RATE*SIZE*CHANNELS/8];
    int sin_size = sizeof(struct sockaddr);
    struct sockaddr_in their_addr_udp;
    int bytes_received;
    
    //This is temporal
    if ((fp = fopen("output_file.wav", "wb")) == NULL)
    {
        perror("Output File");
        exit(1);
    }
    
    int i = 0;
    do
    {
        i++;
        if ((bytes_received = recvfrom(socket_udp, buf, LENGTH*RATE*SIZE*CHANNELS/8, 0, (struct sockaddr *)&their_addr_udp, &sin_size)) == -1)
        {
            perror("recvfrom UDP");
            exit(1);
        }

        fwrite(buf, 1, bytes_received, fp);
        printf("\e[34mRecibiendo por UDP %d bytes\e[0m\n", bytes_received);
    } while (bytes_received == LENGTH*RATE*SIZE*CHANNELS/8);
    
    printf ("\e[31mCerrando Programa\e[0m\n");
    fclose(fp);
    
    return;
}
