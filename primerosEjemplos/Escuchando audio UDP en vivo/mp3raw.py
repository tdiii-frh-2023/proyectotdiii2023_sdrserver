from pydub import AudioSegment

def mp3_to_raw(mp3_file, raw_file):
    # Cargar el archivo MP3
    audio = AudioSegment.from_mp3(mp3_file)

    # Convertir a formato sin comprimir (raw)
    raw_data = audio.raw_data

    # Escribir los datos en un archivo RAW
    with open(raw_file, 'wb') as raw_output:
        raw_output.write(raw_data)

if __name__ == "__main__":
    # Ruta del archivo MP3 de entrada
    mp3_file_path = "enviar.mp3"

    # Ruta del archivo RAW de salida
    raw_file_path = "enviar.raw"

    mp3_to_raw(mp3_file_path, raw_file_path)
