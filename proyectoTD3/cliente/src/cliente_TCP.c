#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include "cliente_TCP.h"

#define MSG_SIZE 1000

int set_tcp (struct sockaddr_in their_addr_tcp)
{
    int socket_tcp;
    
    //Create Socket
    if ((socket_tcp = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket TCP");
        exit(1);
    }
    
    //Connect
    if (connect(socket_tcp, (struct sockaddr *) & their_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("connect TCP");
        exit(1);
    }

    printf ("\e[32mPedido de Conexion hecho\e[0m\n");
    
    return socket_tcp;
}

void tcp_chat_tx (int socket_tcp)
{
    char msg [MSG_SIZE];
    while(1)
    {
        //Wait for user msg
        fgets(msg, MSG_SIZE, stdin);

        //Send via TCP
        if (send(socket_tcp, msg, MSG_SIZE, 0) == -1)
        {
            perror("send UDP Port");
            exit(1);
        }
    }

    return;
}

void tcp_chat_rx (int socket_tcp)
{
    char msg [MSG_SIZE];
    int numbytes = 0;
    
    while(1)
    {
        //Send via TCP
        if ((numbytes = recv(socket_tcp, msg, MSG_SIZE, 0)) == -1)
        {
            perror("send UDP Port");
            exit(1);
        }
        
        if(numbytes != 0)
            printf("%s\n", msg);
    }

    return;
}
