/*
Uso: llamar a cliente y mandar como argumento la IP (127.0.0.0)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/shm.h>
#include <signal.h>

#define TCP_PORT 2236
#define DATA_SIZE 64000
#define INITIAL_PORT 55555
#define MSG_SIZE 1000

int set_tcp (struct sockaddr_in);
int set_udp_port (int);
void receive_udp (int);
void handler_int (int val);
void handler_usr1 (int);
void tcp_chat(int);

char * data;
int child_id;
int new_data = 0;

int main (int argc, char * argv[])
{
    int socket_tcp, socket_udp;
    struct sockaddr_in their_addr_tcp; //Informacion del server
    struct hostent * he; //Direccion a donde se conecta
    int udp_port = 0;

    signal (SIGINT, handler_int);
    signal (SIGUSR2, handler_usr1);

    //Reviso cantidad argumentos
    if (argc != 2)
    {
        perror("argumentos");
        exit(1);
    } 
    
    //Convierto nombre Host a IP
    if ((he = gethostbyname(argv[1]))== NULL)
    {
        perror("gethostbyname");
        exit(1);
    }
    
    //Server Information
    their_addr_tcp.sin_family = AF_INET;
    their_addr_tcp.sin_port = htons(TCP_PORT);
    their_addr_tcp.sin_addr = *(struct in_addr *)he->h_addr;
    bzero(&(their_addr_tcp.sin_zero), 8);
    
    //Set TCP
    socket_tcp = set_tcp(their_addr_tcp);
    
    //Set UDP port
    socket_udp = set_udp_port(socket_tcp);
    
    //Send Commands
    if (!(child_id = fork()))
    {
        tcp_chat(socket_tcp);
        exit(0);
    }

    //Receive and play audio
    receive_udp(socket_udp);
    
    //Close Sockets
    close(socket_tcp);
    close(socket_udp);

    return 0;
}

int set_tcp (struct sockaddr_in their_addr_tcp)
{
    int socket_tcp;
    
    //Create Socket
    if ((socket_tcp = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket TCP");
        exit(1);
    }
    
    //Connect
    if (connect(socket_tcp, (struct sockaddr *) & their_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("connect TCP");
        exit(1);
    }

    printf ("\e[32mPedido de Conexion hecho\e[0m\n");
    
    return socket_tcp;
} 

int set_udp_port(int socket_tcp)
{
    int udp_port = INITIAL_PORT; //Initial UDP Port
    struct sockaddr_in my_addr_udp;
    int socket_udp;
    
    //Create Socket
    if ((socket_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    
    //Set my_addr_udp
    my_addr_udp.sin_family = AF_INET;
    my_addr_udp.sin_port = htons(udp_port);
    my_addr_udp.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr_udp.sin_zero), 8);
    
    //Bind
    while (bind(socket_udp, (struct sockaddr *)&my_addr_udp, sizeof(struct sockaddr))  == -1)
    {
        udp_port++;
        my_addr_udp.sin_port = htons(udp_port);
    }
    
    //Inform Server
    if (send(socket_tcp, &udp_port, sizeof(int), 0) == -1)
    {
        perror("send UDP Port");
        exit(1);
    }

    printf ("\e[34mPuerto UDP: %d\e[0m\n", udp_port);

    return socket_udp;
}

void tcp_chat (int socket_tcp)
{
    char msg [MSG_SIZE];
    while(1)
    {
        //Wait for user msg
        fgets(msg, MSG_SIZE, stdin);
        printf("ESCRIBISTE: %s \n", msg);

        //Send via TCP
        if (send(socket_tcp, msg, MSG_SIZE, 0) == -1)
        {
            perror("send UDP Port");
            exit(1);
        }
    }

    return;
}

void receive_udp(int socket_udp)
{
    char buf[DATA_SIZE*15];
    int i, bytes_acum, bytes_recibidos;
    int sinsize = sizeof(struct sockaddr);
    struct sockaddr_in their_addr;

    FILE *vlcPipe = popen("cvlc -", "w");

    while(1)
    {
        //Reset Variables
        bzero(buf, DATA_SIZE*15);
        i = 0;
        bytes_acum = 0;

        //Receive Audio via UDP
        do
        {
            bytes_recibidos = recvfrom(socket_udp, buf+DATA_SIZE*i, DATA_SIZE, 0, (struct sockaddr *)&their_addr, &sinsize);
            //printf("\033[33mRecibidos %d bytes\033[0m\n", bytes_recibidos);            
            i++;
            bytes_acum += bytes_recibidos;
        }while(bytes_recibidos >= DATA_SIZE);

        //printf("\033[35mTotal paquetes %d\033[0m\n",i);

        //Play Audio
        int escrito = fwrite(buf, sizeof(char), bytes_acum, vlcPipe);
    }
    
    return;
}

void handler_int (int val)
{
    if (child_id)
        kill(child_id, SIGINT);
    printf("\033[31mCerrando Proceso %d...\033[0m\n", getpid());
    exit(0);
}

void handler_usr1 (int val)
{
    new_data = 1;
    return;
}