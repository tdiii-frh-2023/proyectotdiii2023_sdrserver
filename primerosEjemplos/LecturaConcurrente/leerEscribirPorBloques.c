#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <pthread.h>

const char * archivoNombre = "enviar.txt";
int archivoTam = 100;
int bytes_lectura = 10;
int posicion_actual = 0;

int bandera;

void * archivoLeerHilo(void * arg);
void * archivoMostrarHilo(void * arg);

int main(void){
    
    FILE * archivo;
    
    archivo = fopen(archivoNombre,"wb");
    
    char datos[archivoTam];
    for ( int i = 0 ; i < archivoTam ; i ++ ){
        datos[i] = i % 26 + 'A';
    }
    
    size_t bytes_escritos = fwrite(datos,
                                   1,
                                   archivoTam,
                                   archivo);
    
    fclose(archivo);
    
    char buffer[bytes_lectura];
    
    bandera = 0;
    
    pthread_t leer,mandar;
    pthread_create( &leer,
                    NULL,
                    archivoLeerHilo,
                    (void *)buffer
    );
    
    while(posicion_actual <= 50);
    
    pthread_create( &mandar,
                    NULL,
                    archivoMostrarHilo,
                    (void *)buffer
    );
    
    pthread_join(leer, NULL);
    pthread_join(mandar, NULL);
    

    
    fclose(archivo);
    
    return 0;
}

void * archivoLeerHilo(void * arg){
    
    FILE * archivo = fopen(archivoNombre,"rb");
    char * buffer = (char *)arg;
    
    while(1){
        fgets(buffer,bytes_lectura,stdin);
        printf("Leyendo la posicion %d\n",posicion_actual);
        bandera = 1;
        bzero(buffer,bytes_lectura);
        size_t bytes_recibidos = fread( buffer,
                                        1,
                                        bytes_lectura,
                                        archivo
                                    );
        if (posicion_actual >= archivoTam ){
            printf("fin");
        }   else    {
            posicion_actual += bytes_recibidos;
        }
        buffer[posicion_actual] = '\0';        
    }
}

void * archivoMostrarHilo(void * arg){
    
    FILE * archivo = fopen(archivoNombre,"rb");
    char * buffer = (char *)arg;
    
    while(1){
        if(bandera){
            
            bandera = 0;
            printf("Leidos %d bytes ( %s )\n\n",strlen(buffer),buffer);
        }
    }
}
