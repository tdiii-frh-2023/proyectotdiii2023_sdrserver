/*
Uso: llamar a cliente y mandar como argumento la IP (127.0.0.0)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include "cliente_TCP.h"
#include "cliente_UDP.h"
#include "utilidades.h"

#define TCP_PORT 2242

char * data;
int child_1_id = 0;
int child_2_id = 0;

int start (int argc, char **argv, int msgqid)
{
    int socket_tcp, socket_udp;
    struct sockaddr_in their_addr_tcp; //Informacion del server
    struct hostent * he; //Direccion a donde se conecta
    int udp_port = 0;
    int pid_client_subprocess;

    signal (SIGINT, handler_int);

    if(pid_client_subprocess = fork()){
        //La función start debe retornar para que el código de python siga
        //le devuelve el pid del subproceso
        return pid_client_subprocess;
    }

    //Reviso cantidad argumentos
    if (argc != 3)
    {
        perror("argumentos");
        exit(1);
    } 
    
    //Convierto nombre Host a IP
    if ((he = gethostbyname(argv[1]))== NULL)
    {
        perror("gethostbyname");
        exit(1);
    }
    
    //Server Information
    their_addr_tcp.sin_family = AF_INET;
    their_addr_tcp.sin_port = htons(TCP_PORT);
    their_addr_tcp.sin_addr = *(struct in_addr *)he->h_addr;
    bzero(&(their_addr_tcp.sin_zero), 8);
    
    //Set TCP
    socket_tcp = set_tcp(their_addr_tcp);
    
    //Set UDP port
    socket_udp = set_udp_port(socket_tcp);

    choose_name(socket_tcp, argv[2]);
    
    //Send Commands & Chat
    if (!(child_1_id = fork()))
    {
        child_1_id = 0;
        child_2_id = 0;
        tcp_chat_tx(socket_tcp, msgqid);
        exit(0);
    }

    //Receive Msg
    if (!(child_2_id = fork()))
    {
        child_1_id = 0;
        child_2_id = 0;
        tcp_chat_rx(socket_tcp, msgqid);
        exit(0);
    }

    //Receive and play audio
    close(socket_tcp);
    receive_udp(socket_udp);
    
    //Close Sockets
    close(socket_udp);

    return 0;
}
