#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <portaudio.h>

void * hiloGuardarArchivo(void * );
void * hiloReproducirArchivo(void * );

const char * escucharGuardar = "rtl_fm -M wbfm -f 93.3M | tee salida.bin > /dev/null";
const char *archivo = "salida.bin";
const char *guardado = "buffer.bin";


int main() {
    
    pthread_t hilo;
    int id;
    
    pthread_create(&hilo, NULL, hiloGuardarArchivo, &id);
    pthread_create(&hilo, NULL, hiloReproducirArchivo, &id);
    
    while(1);
    
    return 0;
}

void * hiloGuardarArchivo(void * id){
    
    system(escucharGuardar);    
    
    while(1);
    
    return NULL;
}

void * hiloReproducirArchivo(void * id){
    
        
    const int sample_rate = 32000;       // Sample rate in Hz (adjust as needed)
    const int num_channels = 1;          // Mono audio
    
    
    int16_t buffer[1024]; // Adjust the buffer size as needed
    size_t read_count;

    FILE *file;
    FILE *guardar;
    
    printf("\x1b[31m");
    
    while(1){
        
        sleep(1);
        
        file = fopen(archivo, "rb");
        guardar = fopen(guardado, "ab");
        if (!file) {
            return NULL;
        }

        PaError err;
        err = Pa_Initialize();
        if (err != paNoError) {
            printf("PortAudio error: %s\n", Pa_GetErrorText(err));
            return NULL;
        }

        PaStream *stream;
        err = Pa_OpenDefaultStream(&stream, num_channels, num_channels, paInt16, sample_rate, paFramesPerBufferUnspecified, NULL, NULL);
        if (err != paNoError) {
            printf("PortAudio error: %s\n", Pa_GetErrorText(err));
            return NULL;
        }

        err = Pa_StartStream(stream);
        if (err != paNoError) {
            printf("PortAudio error: %s\n", Pa_GetErrorText(err));
            return NULL;
        }


        printf("Llego al while\n");
        
        while ((read_count = fread(buffer, sizeof(int16_t), sizeof(buffer) / sizeof(int16_t), file)) > 0) {
            err = Pa_WriteStream(stream, buffer, read_count);
            if (err != paNoError) {
                printf("PortAudio error: %s\n", Pa_GetErrorText(err));
                break;
            }
            printf("Escribo archivo\n");
            fwrite(buffer,1,sizeof(buffer),guardar);
        }
        
        printf("Salgo del while\n");

        err = Pa_StopStream(stream);
        if (err != paNoError) {
            printf("PortAudio error: %s\n", Pa_GetErrorText(err));
        }

        err = Pa_CloseStream(stream);
        if (err != paNoError) {
            printf("PortAudio error: %s\n", Pa_GetErrorText(err));
        }

        Pa_Terminate();

        fclose(file);
        
        sleep(5);
    }

    
    
    return NULL;
}

    
