/*
Uso: llamar a server
Proceso LECTURA: Lee la SDR escribe datos en archivos .wav.
Proceso Accept: Se queda esperando conexiones nuevas por TCP
Proceso UDP: Recibe Puero UDP por TCP. Envía la información de los archivos por UDP. 
Proceso Recv TCP: Espera comandos y chat.
Proceso Send TCP: Envia por TCP mensaje a su cliente. Esto sirve para que todos reciban el chat
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>

#define DATA_SIZE 64000
#define N 3     //File Size: DATA_SIZE * N
#define MSG_SIZE 1000
#define NAME_SIZE 50
#define TCP_PORT 2242
#define BACKLOG 10
#define MAX_CLIENTS 10

/****************************************************
Descripción: Crea Socket TCP, Bindea y hace el Listen
Argumento: Ninguno
Retorna: Socket TCP del Server
*****************************************************/
int set_tcp (void);

/****************************************************
Descripción: Bucle. Espera mensajes por TCP
Argumento: Socket TCP asociado al cliente
Retorna: Nada
*****************************************************/
void tcp_process (int);

/****************************************************
Descripción: Crea Socket UDP
Argumento: Ninguno
Retorna: Socket UDP
*****************************************************/
int set_udp (void);

/****************************************************
Descripción: Crea Shared Memory para comandos
Argumento: Ninguno
Retorna: Nada
*****************************************************/
void shared_memory_commands (void);

/****************************************************
Descripción: Crea Shared Memory para mensajes
Argumento: Ninguno
Retorna: Nada
*****************************************************/
void shared_memory_msg (void);

/****************************************************
Descripción: Recibe puerto UDP del cliente por TCP
Argumento: Socket TCP asociado al cliente
Retorna: Puerto UDP
*****************************************************/
int get_port(int);

/****************************************************
Descripción: Recibe puerto nombre del cliente por TCP
Argumento: Socket TCP asociado al cliente y Direccion TCP del cliente
Retorna: Nada
*****************************************************/
void get_name(int, struct sockaddr_in);

/****************************************************
Descripción: Lee Wav y lo envia por UDP al cliente asociado
Argumento: Socket UDP y Direccion UDP del cliente
Retorna: Nada
*****************************************************/
void udp_process (int, struct sockaddr_in);

/****************************************************
Descripción: Cierra procesos asociados al cliente desconectado
Argumento: Socket TCP asociado al cliente y Direccion TCP del cliente
Retorna: Nada
*****************************************************/
void close_client(int, struct sockaddr_in);


/*******************************************************
Descripción: Envia mensaje por TCP que obtiene de la SM
Argumento: Socket TCP asociado al cliente
Retorna: Nada
*******************************************************/
void send_tcp_process (int);

/**********************************************************************
Descripción: Handler Señal SIGUSR1. Envia SIGUSR2 a todos los hijos UDP
Argumento: Ninguno
Retorna: Nada
**********************************************************************/
void handler_usr1 (int);

/****************************************************
Descripción: Handler Señal SIGUSR2
Argumento: No utilizado
Retorna: Nada
*****************************************************/
void handler_usr2 (int);

/****************************************************
Descripción: Handler Señal SIGCHLD. Evita Zombies.
Argumento: No utilizado
Retorna: Nada
*****************************************************/
void handler_child (int);

/****************************************************
Descripción: Handler Señal SIGINT. Termina proceso
Argumento: No utilizado
Retorna: Nada
*****************************************************/
void handler_int (int);

/************************************************************************
Descripción: Guarda PID de procesos en vectores. Actualiza contador Hijos
Argumento: PID del nuevo proceso UDP y del nuevo proceso Send TCP
Retorna: Nada
************************************************************************/
void child_id_vector (int udp_pid, int send_tcp_pid);

/****************************************************
Descripción: Identifica los comandos, coloca mensaje en SM y avisa que hay nuevo chat para enviar
Argumento: Nombre cliente y Mensaje
Retorna: Nada
*****************************************************/
void parsear_comando (char *, char *); //Translates Client Command to SDR Command


char buf[DATA_SIZE*N]; //This buffer holds the digitized audio
int new_data = 0;   //1 if children have to read new data in output.wav
int child_id = 0;
int send_tcp_pid = 0;
int childs = 0;     //Number of children alive
int udp_children [MAX_CLIENTS]; //Stores UDP Childrens PID
int send_tcp_children [MAX_CLIENTS]; //Stores SEND TCP Childrens PID
char * data;
char * data_msg;
char * msg_data;
FILE * pipe_fp;
int mp_id; //Main Process ID
int accept_pid = 0; //Accept Process ID
int change_freq = 0; //1 if frequency change is needed
char name [NAME_SIZE]; //Client Name

int main()
{
    int socket_tcp, socket_udp, newfd;
    struct sockaddr_in their_addr_udp, their_addr_tcp;
    int udp_port, command, numbytes = 1, bytes_read;
    int sin_size = sizeof(struct sockaddr);
    const char * create_wav = "sox -t raw -r 16k -e signed-integer -b 16 -c 2 -V1 output.iq output.wav";
    const char * init_sdr = "rtl_fm -M wbfm -f 89.1M";
    const char * output_file_name = "output.iq";
    FILE * output_file;

    //Main Process ID
    mp_id = getpid();

    //Capture SIGINT & SIGUSR2
    signal (SIGINT, handler_int);
    signal (SIGUSR2, handler_usr2);

    //Create Shared Memory
    shared_memory_commands();
    shared_memory_msg();

    if (!(child_id = fork()))
    {
        /*-------------------------------------------------------------------------
        ******************************ACCEPT PROCESS*******************************
        -------------------------------------------------------------------------*/

        //Saves PID
        accept_pid = getpid();

        //Clean Children Vectors
        bzero(udp_children, BACKLOG);
        bzero(send_tcp_children, BACKLOG);

        //Capture SIGUSR1 & SIGCHLD
        signal (SIGUSR1, handler_usr1);
        signal (SIGCHLD, handler_child);

        //Set TCP
        socket_tcp = set_tcp();
        
        //Set UDP
        socket_udp = set_udp();

        while(1)
        {
            //Accept
            if ((newfd = accept(socket_tcp, (struct sockaddr *) &their_addr_tcp, &sin_size)) == -1)
            {
                perror("accept");
                exit(1);
            }
            
            if(childs < MAX_CLIENTS)
            {
                childs++;

                if(!(send_tcp_pid = fork()))
                {
                    /*-------------------------------------------------------------------------
                    ******************************SEND TCP PROCESS*****************************
                    -------------------------------------------------------------------------*/
                    send_tcp_process(newfd);
                    exit(0);
                }

                if(!(child_id = fork()))
                {
                    /*-------------------------------------------------------------------------
                    ********************************UDP PROCESS********************************
                    -------------------------------------------------------------------------*/
                    printf("\033[32m\nConexion Establecida con %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));

                    udp_port = get_port(newfd);

                    get_name(newfd, their_addr_tcp); 

                    if(!(child_id = fork()))
                    {
                        /*-------------------------------------------------------------------------
                        *****************************TCP RECV PROCESS******************************
                        -------------------------------------------------------------------------*/
                        //Receive Commands TCP
                        tcp_process(newfd);

                        //Connection Lost
                        close_client(newfd, their_addr_tcp);
                    }       
 
                    //Receive Port
                    their_addr_udp = their_addr_tcp;
                    their_addr_udp.sin_port = htons(udp_port);

                    //Send UDP
                    while(1)
                    {
                        udp_process(socket_udp, their_addr_udp);
                    }
                }

                child_id_vector(child_id, send_tcp_pid);
                child_id = 0;
            }
            else
            {
                close(newfd);
                printf("\033[31mConexion Rechazada: Maximo de Clientes Alcanzado\033[0m\n");
            }

        }
        
    }
/*-------------------------------------------------------------------------
********************************SDR PROCESS********************************
-------------------------------------------------------------------------*/

    printf("\033[36m------CONFIGURANDO SDR------\033[0m\n");

    if ((pipe_fp = popen(init_sdr, "r")) == NULL)
    {
        perror("Error opening pipe");
        return EXIT_FAILURE;
    }

    if ((output_file = fopen(output_file_name, "wb")) == NULL)
    {
        perror("Error opening output file");
        pclose(pipe_fp);
        return EXIT_FAILURE;
    }
    
    while(1)
    {
        //Get Audio
        fflush(pipe_fp);
        bzero(buf,DATA_SIZE*N);
        bytes_read = fread(buf, 1, (DATA_SIZE*N),pipe_fp);
        rewind(output_file);
        fwrite(buf, 1, bytes_read, output_file);

        //Create .wav File
        system(create_wav);

        //New data available
        kill (child_id, SIGUSR1);

        //Change Frequency if required
        if(change_freq)
        {
            change_freq = 0;
            pclose(pipe_fp);
            if ((pipe_fp = popen(data, "r")) == NULL)
            {
                perror("Error opening pipe");
                exit(1);
            }
        }
    }

    return 0;
}


int set_tcp (void)
{
    int socketfd_tcp;
    struct sockaddr_in my_addr_tcp;
    int reuseaddr = 1;
    
    //Create Socket
    if ((socketfd_tcp = socket(AF_INET, SOCK_STREAM ,0)) == -1) 
    {
        perror("socket TCP");
        exit(1);
    }

    // setsockopt permite reutilizar varias veces el mismo BIND
    setsockopt(socketfd_tcp, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int));
    
    //set my__addr_tcp
    my_addr_tcp.sin_family = AF_INET;
    my_addr_tcp.sin_port = htons(TCP_PORT);
    my_addr_tcp.sin_addr.s_addr = INADDR_ANY;
    bzero(& (my_addr_tcp.sin_zero), 8);
    
    //Bind Port
    if (bind(socketfd_tcp, (struct sockaddr *) & my_addr_tcp, sizeof(struct sockaddr)) == -1)
    {
        perror("bind TCP");
        exit(1);
    }
    
    //Indicate Queue size
    if (listen(socketfd_tcp, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }

    printf("\033[32mTCP Creado en puerto %d...\033[0m\n", TCP_PORT);

    return socketfd_tcp;
}

void shared_memory_commands (void)
{
    key_t key_sh;
    int shmid;

    //Create Shared Memory
    if ((key_sh = ftok("proyecto_server.c", 'R')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((shmid = shmget (key_sh, MSG_SIZE, 0644 | IPC_CREAT)) == -1)
    {
        perror("shmget");
        exit(1);
    }

    if ((data = shmat(shmid, 0, 0)) == (char *) (-1))
    {
        perror("shmat");
        exit(1);
    }

    return;
}

void shared_memory_msg (void)
{
    key_t key_sh;
    int shmid;

    //Create Shared Memory
    if ((key_sh = ftok("proyecto_server.c", 'Y')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((shmid = shmget (key_sh, MSG_SIZE, 0644 | IPC_CREAT)) == -1)
    {
        perror("shmget");
        exit(1);
    }

    if ((data_msg = shmat(shmid, 0, 0)) == (char *) (-1))
    {
        perror("shmat");
        exit(1);
    }

    return;
}

void tcp_process (int newfd)
{
    int numbytes = 0;
    char msg [MSG_SIZE];
    char final_msg [MSG_SIZE+NAME_SIZE+2];

    //Wait for commands
    do
    {
        if ((numbytes = recv(newfd, msg, MSG_SIZE, 0)) == -1)
        {
            perror("recv TCP");
            exit(1);
        }
        printf("Recibidos %d bytes\n", numbytes);

        if(numbytes != 0)
            parsear_comando(name, msg);
    }while(numbytes != 0);

    return;
}  

int set_udp ()
{
    int socketfd_udp;

    //Create Socket
    if ((socketfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket UDP");
        exit(1);
    }

    return socketfd_udp;
}

void send_tcp_process(int newfd)
{
    new_data = 0;

    while(1)
    {
        if(new_data)
        {
            new_data = 0;

            //Send msg via TCP
            if (send(newfd, data_msg, MSG_SIZE, 0) == -1)
            {
                perror("send TCP");
                exit(1);
            }
        }
    }
}

int get_port(int newfd)
{
    int udp_port;

    if ((recv(newfd, &udp_port, sizeof(int), 0)) == -1)
    {
        perror("Receive Port UDP");
        exit(1);
    }
    printf("\033[34mRecibido Puerto UDP: %d\033[0m\n",udp_port);

    return udp_port;
}

void get_name(int newfd, struct sockaddr_in their_addr_tcp)
{
    int bytes_read;

    if ((bytes_read = recv(newfd, name, NAME_SIZE, 0)) == -1)
    {
        perror("Receive Name");
        exit(1);
    }
    
    if(bytes_read == 0)
    {
        printf("\033[31mConexion perdida desde %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));
        kill(send_tcp_pid, SIGKILL);
        sleep(1);
        exit(0);
    }

    printf("\033[34mNombre del Cliente: %s\033[0m\n", name);

    return;
}

void udp_process (int socket_udp, struct sockaddr_in their_addr_udp)
{   
    int i=0, bytes_leidos;
    char * input_file = "output.wav";
    FILE * fd;

    if (new_data == 1)
    {
        new_data = 0;

        //Open file
        if ((fd = fopen(input_file, "rb")) == NULL)
        {
            perror("Open File");
            exit(1);
        }

        //Send audio via UDP
        while ((bytes_leidos = fread(buf, 1, DATA_SIZE, fd)) > 0)
        {
            if ((sendto(socket_udp, buf, bytes_leidos, 0, (struct sockaddr *)&their_addr_udp, sizeof(struct sockaddr))) == -1)
            {
                perror("sendto");
                return;
            }
            usleep(1000);

            //printf("\033[33mEnviando %d bytes al puerto %d\033[0m\n", bytes_leidos, ntohs(their_addr_udp.sin_port));
        }

        //Close file
        fclose(fd);

    }
    
    return;
}

void parsear_comando(char *name, char * msg)
{
    char * token;
    token = strtok(msg, "/");
    char sdr_command[MSG_SIZE];
    char * modeloFM = "rtl_fm -M wbfm -f %s";
    char final_msg [NAME_SIZE+MSG_SIZE+2];
    

    //Command Style: /fmradio/<frecuencia en MHz>/
    if(strcmp(token, "fmradio"))
    {
        //Not a Command
        sprintf(final_msg, "%s: %s", name, msg);
        strcpy(data_msg, final_msg);
        kill(accept_pid, SIGUSR2);
        return;
    }
    
    bzero(sdr_command,MSG_SIZE);
    token = strtok(NULL, "/");
    snprintf(sdr_command, MSG_SIZE, modeloFM, token);
    sdr_command[strlen(sdr_command)-1] = ' ';

    printf("\033[35mEl comando recibido es [%s]\033[0m\n",sdr_command);

    //Msg to send
    sprintf(final_msg, "\033[36m%s cambio la radio a %s\033[0m\n", name, token);
    strcpy(data_msg, final_msg);
    kill(accept_pid, SIGUSR2);
    
    strcpy(data, sdr_command);

    kill(mp_id, SIGUSR2);

    return;
}

void close_client(int newfd, struct sockaddr_in their_addr_tcp)
{
    printf("\033[31mConexion perdida desde %s\033[0m\n", inet_ntoa(their_addr_tcp.sin_addr));

    //Kill UDP Process
    kill(getppid(), SIGKILL);

    sleep(1);

    //Kill SEND TCP Process
    kill(send_tcp_pid, SIGKILL);

    //Kill RECV TCP Process
    close (newfd);
    exit(0);
}


void handler_usr1 (int val)
{
    int i = 0;

    for(i=0; i<MAX_CLIENTS; i++)
    {
        if(udp_children[i] != 0)
            kill(udp_children[i], SIGUSR2);
    }

    return;
}

void handler_usr2 (int val)
{
    int i;

    new_data = 1;
    change_freq = 1;

    //If Accept Process receives SIGUSR2, sends SIGUSR 2 to every Send TCP Process
    if(getpid() == accept_pid)
    {
        for(i=0; i<MAX_CLIENTS; i++)
        {
            if(send_tcp_children[i] != 0)
                kill(send_tcp_children[i], SIGUSR2);
        }
    }

    return;
}

void handler_child (int val)
{
    wait(0);
    return;
}

void handler_int (int val)
{
    printf("\033[31mCerrando Proceso %d...\033[0m\n", getpid());
    exit(0);
}

void child_id_vector (int udp_new_id, int send_tpc_new_id)
{
    int i = 0;

    //Removes Process no longer alive
    for(i=0; i<MAX_CLIENTS; i++)
    {
        if(udp_children[i] != 0)
        {
            if(kill(udp_children[i], 0)) //Checks if procees is alive
            {
                udp_children[i] = 0;
                send_tcp_children[i] = 0;
            }
        }
    }

    //Puts new PID struct in "children" vector
    i=0;
    while(udp_children[i] != 0)
    {
        i++;
    }

    udp_children[i] = udp_new_id;
    send_tcp_children[i] = send_tpc_new_id;

    //Check how many children alive
    childs = 0;
    for(i=0; i<MAX_CLIENTS; i++)
    {
        if(udp_children[i] != 0)
            childs++;
    }
}